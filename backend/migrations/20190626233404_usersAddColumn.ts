import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {
    const havaFacebookId = await knex.schema.hasColumn('users', 'facebook_Id')
    const haveGoogleId = await knex.schema.hasColumn('users', 'google_Id')
    const haveEmail=await knex.schema.hasColumn('users', 'email')
    if (!havaFacebookId) {
        await knex.schema.alterTable('users', (table) => {
            table.string('facebook_Id')
        })
    }
    if (!haveGoogleId) {
        await knex.schema.alterTable('users', (table) => {
            table.string('google_Id')
        })
    }
    if (!haveEmail) {
        await knex.schema.alterTable('users', (table) => {
            table.string('email')
        })
    }
    return
};

exports.down = function (knex: Knex): Promise<any> {
    return knex.schema.table('users', (table) => {
        table.dropColumns('facebook_Id', 'google_Id','email')
    })
};
