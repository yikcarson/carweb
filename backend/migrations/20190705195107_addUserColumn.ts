import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {
    const has = await knex.schema.hasColumn("carInfo", 'user_id')
    if (!has) {
        await knex.schema.alterTable("carInfo", (table) => {
            table.integer('user_id').unsigned
            table.foreign('user_id').references('users.id')
        })
    }
    return
};

exports.down = async function (knex: Knex): Promise<any> {
    return await knex.schema.alterTable("carInfo", (table) => {
        table.dropColumn('user_id')
    })
};
