import * as Knex from "knex";


exports.up = async function (knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable('brand')&& knex.schema.hasTable('website')&& knex.schema.hasTable('carInfo')&& knex.schema.hasTable('users')&& knex.schema.hasTable('picture')&& knex.schema.hasTable('rental')&& knex.schema.hasTable('carAdvert')&& knex.schema.hasTable('exercises') && knex.schema.hasTable('favorites')
    if (!hasTable) {
    await knex.schema.createTable('brand', (table)=>{
        table.increments();
        table.string('brandName')
        table.timestamps(false, true);
    })
    await knex.schema.createTable('website', (table)=>{
        table.increments();
        table.string('websiteName');
        table.string('websiteLink');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('carInfo', (table)=>{
        table.increments();
        table.integer('brand_id');
        table.foreign('brand_id').references('brand.id');
        table.string('name').notNullable();
        table.integer('year').notNullable();
        table.boolean('isAuto')
        table.integer('seats').notNullable();
        table.integer('volume').notNullable();
        table.text('detail');
        table.integer('website_id');
        table.foreign('website_id').references('website.id')
        table.integer('price');
        table.string('url');
        table.string("imgLink")
        table.string('contactName');
        table.string('contactNo');
        table.boolean('isSold').notNullable();
        table.integer('km');
        table.boolean('rental').defaultTo('false');
        table.date('updateDate');
        table.timestamps(false, true);
    });

    await knex.schema.createTable('users', (table)=>{
        table.increments();
        table.string('username').notNullable();
        table.string('password').notNullable();
        table.timestamps(false, true);
    })

    await knex.schema.createTable('picture', (table)=>{
        table.increments();
        table.integer('car_id');
        table.foreign('car_id').references('carInfo.id')
        table.string('imgLink').notNullable();
        table.timestamps(false, true);
    })

    await knex.schema.createTable('rental', (table)=>{
        table.increments();
        table.integer('User_id');
        table.foreign('User_id').references('users.id')
        table.integer('car_id');
        table.foreign('car_id').references('carInfo.id');
        table.date('start_date')
        table.integer('days').notNullable();
        table.integer('rental_userId');
        table.foreign('rental_userId').references('users.id')
        table.integer('RentalPrice').notNullable();
        table.boolean('isPay').notNullable().defaultTo(false);
        table.string('paymentToken')

        table.timestamps(false, true);
    })

    await knex.schema.createTable('carAdvert', (table)=>{
        table.increments();
        table.integer('car_id');
        table.foreign('car_id').references('carInfo.id');
        table.integer('user_id');
        table.foreign('user_id').references('users.id');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('favorites', (table)=>{
        table.increments();
        table.integer('car_id');
        table.foreign('car_id').references('carInfo.id');
        table.integer('user_id');
        table.foreign('user_id').references('users.id');
        table.timestamps(false, true);
    })
}
    
}


exports.down = async function (knex: Knex): Promise<any> {
    await knex.schema.dropTable('favorites');
    await knex.schema.dropTable('carAdvert');
    await knex.schema.dropTable('rental');  
    await knex.schema.dropTable('picture');
    await knex.schema.dropTable('users');
    await knex.schema.dropTable('carInfo');
    await knex.schema.dropTable('website');
    await knex.schema.dropTable('brand');
}