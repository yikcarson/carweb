import * as passport from 'passport';
import * as passportJWT from 'passport-jwt';
import jwt from './jwt';
import { UserService } from './services/UserService';

const  JWTStrategy = passportJWT.Strategy;
const {ExtractJwt} = passportJWT;

export const initializePassport = (userService: UserService) => {
  passport.use(new JWTStrategy({
          secretOrKey: jwt.jwtSecret,
          jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
      },async (payload,done)=>{
          const user = await userService.getUserByUsername(payload.username);
          if(user){
              return done(null,payload);
          } else {
              return done(new Error("User not Found"),null);
          }
      })
  );
}