const Porsche = ["911", "Boxster", "Cayman", "Panamera", "Cayenne", "Macan"]
const Toyota = ["Wish", "Yaris", "TownAce", "Voxy", "Sienta", "SA", "Hiace", "RAV4 EV"
    , "Ractis", "Prius", "Previa", "Porte", "Noah", "Cruiser", "Corolla Verso", "Estima", "ALPHARD"]

const Audi = ["A1", "A3", "A4", "A5", "A6", "A7", "A8", "S3", "S4", "S5", "S6", "S7", "S8", "Q3", "RS3", "RS4", "RS5",
    "RS6", "RS7", "R8", "TT", "Q2", "Q3", "Q5", "Q7"]

const BMW = ['M2', "M3", "M5", "x3", "x4", "x6", "520d", "523i", "525i", "520i"]
const Honda = ["TypeR", "VEZEL", "JAZZ", "FREED", "NSX", "ODYSSEY", "STEPWGN", "JADE RS", "CR-V"]
const MercedesBenz = ["A200", "A250", "B200", "C200 Facelift", "C300 Facelift", "E200 Avantgarde", "E300 Avantgarde", "E350"]


export const carModel = { Porsche, Toyota, Audi, BMW, Honda, MercedesBenz }