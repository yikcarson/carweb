export interface car1 {
    name: string
    Year: null | number
    isAtuo: boolean
    seat: number | null
    Volume: number | null
    Detail: string | null
    website: string
    picture1: string | null
    picture2: string | null
    picture3: string | null
    picture4: string | null
    price: number | null
    km: null | number
    brand: string,
    isSold: boolean
    url: string,
    update: number | null
    contactName: string,
    contactNo: number | null
}

export interface dchuu {
    name: string
    Year: number | null
    isAtuto: boolean
    seat: number | null
    Volume: number | null,
    Detail: string
    website: string
    picture1: string
    picture2: string
    picture3: string
    picture4: string
    price: number | null
    km: number | null
    brand: string
    isSold: boolean
    url: string,
}


export interface carData {
    id: number,
    name: string,
    year: number,
    isAuto: boolean,
    seats: number | null,
    volume: number | null,
    detail: string | null,
    url: string,
    contactName: string | null,
    contactNo: number | null,
    isSold: boolean,
    km: number | null,
    brandName: string | null,
    websiteName: string
}

export interface queryDataFromFrontend {
    brand: string | null,
    seats: number | undefined,
    isAuto: boolean | undefined,
    startPrice: number | undefined,
    endPrice: number | undefined,
    startEvol: number | undefined
    endEvol: number | undefined,
    year: number | undefined
}

export interface queryBrandMatch {
    match: {
        brandName: string
    }
}
export interface queryNameMatch {
    match: {
        name: string
    }
}

export interface querySeatMatch {
    match: {
        seats: number
    }
}

export interface queryYearMatch {
    match: {
        year: number
    }
}
export interface queryIsAutoMatch {
    match: {
        isAuto: boolean
    }
}

export interface queryPriceRange {
    range: {
        price: {
            gte: number,
            lte: number | null
        }
    }
}

export interface queryEvolRange {
    range: {
        volume: {
            gte: number,
            lte: number | null
        }
    }
}

export interface mustType {
    match: {
        [key: string]: string
    }
}

export interface ToElasticQueryData {
    query: {
        bool: {
            must: [queryYearMatch | queryPriceRange | queryBrandMatch | querySeatMatch | queryIsAutoMatch | queryEvolRange | queryNameMatch | null]
        }
    },
    size: number
}


export interface postcarBody {
    username: string,
    brand: string,
    carname: string,
    year: string,
    isAuto: string,
    seats: string,
    volume: string,
    detail: Text,
    price: string,
    contactName: string,
    contactNo: string,
    km: string,
    rental: string
    rentalPrice: string
    rental_day: string
}


export interface payment {
    rentalCarName: string;
    imgLink: string;
    username: string;
    Days: number;
    price: number;
    //add 
    car_id:number;
    startDay:Date
}

export interface paymentToken {
    card: Object
    client_ip: string
    created: number
    email: string
    id: string
    livemode: boolean
    object: string
    type: string
    used: boolean
}