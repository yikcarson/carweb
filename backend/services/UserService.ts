import * as Knex from 'knex'
import { hashPassword, checkPassword } from './hash';

export class UserService {
    constructor(private knex: Knex) {

    }
    createUserByLocal = async (username: string, password: string) => {
        if (username == '' || password == '') {
            throw new Error(`error username or password`)
        }
        const hasUser = await this.knex('users').where('username', username).first()
        if (hasUser) {
            throw new Error(`${username} is exists`)
        } else {
            const userId = await this.knex('users').insert({
                username,
                password: await hashPassword(password)
            }).returning(['id'])
            return userId[0]
        }
    }

    getUserByUsername = (username: string) => {
        return this.knex.select('*').from('users').where('username', username).first()
    }

    checkUserPassword = async (username: string, password: string) => {
        const user = await this.knex.select('*').from('users').where('username', username).first()
        if (user) {
            if (await checkPassword(password, user.password)) {
                return user
            } else {
                return
            }
        }
    }
    createUserByFacebook = async (username: string, facebook_Id: string) => {
        const ids = await this.knex('users').insert({
            username,
            password: '',
            facebook_Id,
        }).returning('id')
        return {
            id: ids[0],
            username,
        }
    }

    getUserByFacebookId = (facebook_id: string) => {
        return this.knex.select('*').from('users').where('facebook_Id', facebook_id).first()
    }

    getPostCarbyUsername = async (username: string) => {
        const userId = await this.getUserByUsername(username)
        const result = await this.knex.select("carInfo.id", "carInfo.name", "year", "isAuto", "seats", "volume", "detail", "price", "url", "contactName", "contactNo", "isSold", "km", "brandName", "websiteName", "imgLink", "updateDate").from("carInfo")
            .leftJoin('brand', 'carInfo.brand_id', 'brand.id')
            .join("website", "carInfo.website_id", 'website.id')
            .where('user_id', userId.id)
        return result
    }

    addFavortiesPost = async (car_id: number, username: string) => {
        try {
            const userId = await this.getUserByUsername(username)
            console.log(userId, car_id)
            const hasRecond = await this.knex('favorites').where('car_id', car_id).andWhere('user_id', userId.id).first()
            if (hasRecond) {
                return { 'msg': 'Not success' }
            }
            const favId = await this.knex.insert({
                car_id,
                user_id: userId.id
            }).into('favorites').returning('id')
            console.log(favId)
            if (favId.length > 0) {
                return { 'msg': 'success', username, favId: favId[0] }
            }
           
        } catch (error) {
            return error
        }

    }
    deletFavortiesPost = async (car_id: number, username: string) => {
        try {
            const userId = await this.getUserByUsername(username)
            const hasRecond = this.knex('favorites').where('car_id', car_id).andWhere('user_id', userId.id).first()
            if (await hasRecond) {
                const favId = await hasRecond.del().returning('id')
                if (favId.length > 0) {
                    return { 'msg': 'success' }
                }
            } else {
                return { 'msg': 'Not success' }
            }
        } catch (error) {
            return error
        }
    }

    getFavortiesPost = async (username: string, ) => {
        const userId = await this.getUserByUsername(username)
        const FavId = await this.knex.select("carInfo.id", "carInfo.name", "year", "isAuto", "seats", "volume", "detail", "price", "url", "contactName", "contactNo", "isSold", "km", "brandName", "websiteName", "imgLink", "updateDate","rental").from('carInfo')
            .innerJoin('favorites', 'carInfo.id', 'favorites.car_id')
            .leftJoin('brand', 'carInfo.brand_id', 'brand.id')
            .join("website", "carInfo.website_id", 'website.id')
            .where('favorites.user_id', userId.id)
        return FavId
    }


    getRentalRecond =async (username:string) => { 
        const userId = await this.getUserByUsername(username)
        const RentalId = await this.knex.select("carInfo.id", "carInfo.name", "year", "isAuto", "seats", "volume", "detail", "rental.RentalPrice", "url", "contactName", "contactNo", "isSold", "km", "brandName", "websiteName", "imgLink", "updateDate","rental.start_date","rental.days").from('carInfo')
            .innerJoin('rental', 'carInfo.id', 'rental.car_id')
            .leftJoin('brand', 'carInfo.brand_id', 'brand.id')
            .join("website", "carInfo.website_id", 'website.id')
            .where("rental.rental_userId", userId.id)
            console.log(RentalId)
        return RentalId
    }
}
