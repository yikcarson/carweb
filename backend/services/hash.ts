import * as bcrypt from 'bcrypt'

const SALT_ROUNDS = 10;

export const hashPassword = async (password: string) => {
    const hash = await bcrypt.hash(password, SALT_ROUNDS)
    return hash
}

export const checkPassword = async (plainPassword: string, hashPassword: string) => {
    const match = await bcrypt.compare(plainPassword, hashPassword)
    return match
}