import * as knex from 'knex'
import axios from 'axios'
// import fetch from 'node-fetch'
import { carData, queryDataFromFrontend, ToElasticQueryData, queryBrandMatch, queryIsAutoMatch, querySeatMatch, queryPriceRange, queryYearMatch, queryEvolRange, queryNameMatch } from '../model/model';
import { carModel } from '../model/carModel';
import * as jsonFile from 'jsonfile';
// const timer = (ms: number) => new Promise(res => setTimeout(res, ms));


export class ElasticSearchService {
    constructor(private knex: knex) {
    }

    loadDataToElasticSearch = async () => {
        try {
            const carDatas: carData[] = await this.knex.select("carInfo.id", "carInfo.name", "year", "isAuto", "seats", "volume", "detail", "price", "url", "contactName", "contactNo", "isSold", "km", "brandName", "websiteName", "imgLink", "updateDate", "user_id").from("carInfo")
                .leftJoin('brand', 'carInfo.brand_id', 'brand.id')
                .join("website", "carInfo.website_id", 'website.id')
            console.log(carDatas.length)
            for (let i = 0; i < carDatas.length; i++) {
                const res = await axios({
                    method: "POST",
                    url: `${process.env.elasticSearchService}/car/doc/${carDatas[i].id}`,
                    data: carDatas[i],
                    headers: { "Authorization": `${process.env.elasticSearchLogin}` }
                })
                console.log(res.statusText + i)

            }
            console.log(carDatas.length)
            return 'finish'
        } catch (error) {
            return error
        }

    }

    quickSearch = async (queryData: queryDataFromFrontend) => {
        console.log(queryData);
        if (queryData == null) {
            throw new Error("no any input")
        }
        const searchCondition: ToElasticQueryData = { query: { bool: { must: [null] } }, size: 10000 }

        if (queryData.brand) {
            const brand: queryBrandMatch = { match: { brandName: queryData.brand } }
            searchCondition.query.bool.must.push(brand)
        }
        if (queryData.isAuto) {
            const isAuto: queryIsAutoMatch = { match: { isAuto: queryData.isAuto } }
            searchCondition.query.bool.must.push(isAuto)
        }
        if (queryData.seats) {
            const seats: querySeatMatch = { match: { seats: queryData.seats } }
            searchCondition.query.bool.must.push(seats)
        }
        if (queryData.year) {
            const year: queryYearMatch = { match: { year: queryData.year } }
            searchCondition.query.bool.must.push(year)
        }

        // Object.keys(queryData).map((key) => {
        //     searchCondition.query.bool.must.push({
        //         match: {
        //             [key]: queryData[key]
        //         }
        //     })
        // })
        if ((queryData.startEvol || queryData.startEvol == 0) && queryData.endEvol && queryData.endEvol >= queryData.startEvol) {
            const Price: queryEvolRange = { range: { volume: { gte: queryData.startEvol, lte: queryData.endEvol } } }
            searchCondition.query.bool.must.push(Price)
        } else if ((queryData.startEvol || queryData.startEvol == 0) && queryData.endEvol && queryData.endEvol < queryData.startEvol) {
            throw new Error('error price')
        }
        if ((queryData.startPrice || queryData.startPrice == 0) && queryData.endPrice && queryData.endPrice >= queryData.startPrice) {
            const Price: queryPriceRange = { range: { price: { gte: queryData.startPrice, lte: queryData.endPrice } } }
            searchCondition.query.bool.must.push(Price)
        } else if ((queryData.startPrice || queryData.startPrice == 0) && queryData.endPrice && queryData.endPrice < queryData.startPrice) {
            throw new Error('error price')
        }
        // console.log(JSON.stringify(searchCondition))
        // const afterquery = encodeURI(JSON.stringify(searchCondition.query))
        carModel[queryData.brand!]
        try {
            const datas = {}
            // const promiseData = carModel[queryData.brand!].map(async (brand: any, index: number) => {
            //     if (index != 0) {
            //         searchCondition.query.bool.must.pop()
            //     }
            //     const models: queryNameMatch = { match: { name: brand } }
            //     searchCondition.query.bool.must.push(models)
            //     console.log(JSON.stringify(searchCondition))

            //     const res = await axios({
            //         method: "GET",
            //         url: `${process.env.elasticSearchService}/car/_search/`,
            //         data: searchCondition,
            //     })
            //     return ({ brand: brand, hits: res.data.hits})
            //     //elastic search 6.7
            //     // if (res.data.hits.total != 0) {
            //     //     const data = res.data.hits.hits
            //     //     data.sort((a: number, b: number) => { return a["_source"]["year"] - b["_source"]['year'] })
            //     //     datas[carModel[queryData.brand!][i]] = { datas:data, amount: res.data.hits.total.value, label: [], prices: [], km: [] }
            //     // }
            //     //elastic search 7.0
            //     // if (res.data.hits.total.value != 0) {
            //     //     const data = res.data.hits.hits
            //     //     data.sort((a: number, b: number) => { return a["_source"]["year"] - b["_source"]['year'] })
            //     //     datas[brand] = { datas: data, amount: res.data.hits.total.value, label: [], prices: [], km: [] }
            //     // }
            // })

            // const result = await Promise.all(promiseData)

            // result.map((r: any) => {
            //     console.log(r )
            //     if (r.hit.total.value != 0) {
            //         const data = r.hit
            //         data.sort((a: number, b: number) => { return a["_source"]["year"] - b["_source"]['year'] })
            //         datas[r.brand] = { datas: data, amount: r.hit.total.value, label: [], prices: [], km: [] }
            //     }
            // })



            // const results = carModel[queryData.brand!].map(async (brand: string, i: number)=>{
            //     if (i != 0) {
            //         searchCondition.query.bool.must.pop()
            //     }
            //     const models: queryNameMatch = { match: { name: carModel[queryData.brand!][i] } }
            //     searchCondition.query.bool.must.push(models)
            //     console.log(JSON.stringify(searchCondition))

            //     const res = await axios({
            //         method: "GET",
            //         url: `${process.env.elasticSearchService}/car/_search/`,
            //         data: searchCondition,
            //         headers: { "Authorization": `${process.env.elasticSearchLogin}` }
            //     })

            //     return { brand, hits: res.data.hits }

            // })

            // const data = await Promise.all(results)
            // data.map((d: any) => {
            //     if (d.total.value != 0) {
            //         const data = d.hits
            //         data.sort((a: number, b: number) => { return a["_source"]["year"] - b["_source"]['year'] })
            //         datas[d.brand] = { datas:data, amount: d.total.value, label: [], prices: [], km: [] }
            //     }
            // })

            console.log(datas)

            for (let i = 0; i < carModel[queryData.brand!].length; i++) {
                if (i != 0) {
                    searchCondition.query.bool.must.pop()
                }
                const models: queryNameMatch = { match: { name: carModel[queryData.brand!][i] } }
                searchCondition.query.bool.must.push(models)
                console.log(JSON.stringify(searchCondition))

                const res = await axios({
                    method: "GET",
                    url: `${process.env.elasticSearchService}/car/_search/`,
                    data: searchCondition,
                    headers: { "Authorization": `${process.env.elasticSearchLogin}` }
                })
                // //     //elastic search 6.7
                // //     // if (res.data.hits.total != 0) {
                // //     //     const data = res.data.hits.hits
                // //     //     data.sort((a: number, b: number) => { return a["_source"]["year"] - b["_source"]['year'] })
                // //     //     datas[carModel[queryData.brand!][i]] = { datas:data, amount: res.data.hits.total.value, label: [], prices: [], km: [] }
                // //     // }
                // //      //elastic search 7.0
                if (res.data.hits.total.value != 0) {
                    const data = res.data.hits.hits
                    data.sort((a: number, b: number) => { return a["_source"]["year"] - b["_source"]['year'] })
                    datas[carModel[queryData.brand!][i]] = { datas: data, amount: res.data.hits.total.value, label: [], prices: [], km: [] }
                }
            }

            // console.log(datas)
            // const outputData = {
            //     "TypeR":{
            //         amount:Number,
            //         datas:[],

            //     }
            // }


            for (let key in datas) {
                console.log(datas[key])
                for (let data of datas[key]["datas"]) {
                    // console.log(data)
                    datas[key].label.push(data["_source"]["year"]),
                        datas[key].prices.push(data["_source"]["price"]),
                        datas[key].km.push(data["_source"]["km"])
                }
            }
            // await jsonFile.writeFile('./reuslt.json', datas)
            return datas
        } catch (error) {
            return error
        }
    }

    customizeSearch = async (keywords: string) => {
        console.log(keywords)
        try {
            const ik_smart = await axios({
                method: "GET",
                url: `${process.env.elasticSearchService}/_analyze/`,
                data: { "text": keywords, "analyzer": "ik_smart" }
            })
            if (ik_smart.data.tokens.length == 0) {
                return { msg: "Not Correct Input" }
            }
        } catch (error) {
            return error
        }


    }

    createJson = async () => {
        try {
            const carDatas: carData[] = await this.knex.select("carInfo.id", "carInfo.name", "year", "isAuto", "seats", "volume", "detail", "price", "url", "contactName", "contactNo", "isSold", "km", "brandName", "websiteName", "imgLink", "updateDate", "user_id", "carInfo.rental").from("carInfo")
                .leftJoin('brand', 'carInfo.brand_id', 'brand.id')
                .join("website", "carInfo.website_id", 'website.id')
            console.log(carDatas[0])
            for (let cardata of carDatas) {
                console.log(cardata)
                await jsonFile.writeFile('./elastic.json', { "index": { "_index": "car", "_id": `${cardata.id}`, "_type": "doc" } }, { flag: "a+" })
                await jsonFile.writeFile('./elastic.json', cardata, { flag: "a+" })
            }
            return console.log('finish')
        } catch (error) {
            console.log(error)
        }
    }

    // uploadToElastic = async () => {
    //     const data = await fs.promises.readFile('./elastic.json')
    //     console.log(data)
    //     const res = await axios({
    //         method: "POST",
    //         url: 'http://localhost:9200/_bulk',
    //         headers: {
    //             "Content-Type": "application/json"
    //         },
    //         data: JSON.stringify(data)
    //     })
    //     return res
    // }

}
