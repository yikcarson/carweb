import * as Knex from 'knex';
import * as Stripe from 'stripe'
import { payment, paymentToken } from '../model/model';

const stripe = new Stripe(`${process.env.stripeKey}`)

export class RentalService {
    constructor(private knex: Knex) {

    }
    getRentalCar = async () => {
        const datas = await this.knex.select("car_id", "days", "RentalPrice", "users.username", "carInfo.name", "year", "isAuto", "seats", "volume", "detail", "price", "imgLink", "contactName", "contactNo", "km", "brandName", "rental").from("rental")
            .leftJoin('users', 'rental.User_id', 'users.id')
            .leftJoin('carInfo', 'rental.car_id', 'carInfo.id')
            .leftJoin('brand', 'carInfo.brand_id', 'brand.id')
            .where("rental.isPay", false)

        // for (let data of datas) {
        //     data['rental'] = true
        // }
        return datas
    }

    payment = async (product: payment, token: paymentToken) => {
        const record = await this.knex.select('*').from('rental').where('car_id', product.car_id).first()
        console.log(product)
        if (record.isPay == true) {
            console.log('payture')
            throw new Error('This car is already rental')
        }
        const customer = await stripe.customers.create({
            email: token.email,
            source: token.id
        })
        const charges = await stripe.charges.create({
            amount: product.price * 100,
            currency: "hkd",
            customer: customer.id
        })

        const user = await this.knex.select('*').from('users').where('username', product.username).first()
        await this.knex('rental').update({ isPay: true, rental_userId: user.id,start_date:product.startDay }).where('car_id', product.car_id)
        await this.knex('carInfo').update({rental:false}).where('id',product.car_id)
        return { 'msg': 'Paymen Success', charges }
    }
}