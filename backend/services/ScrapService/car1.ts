import * as puppeteer from 'puppeteer'
// import { car1Detail } from './model';
import * as jsonFile from 'jsonFile'
import moment = require('moment');
import { car1 } from '../../model/model';

const scrapAllUrls = async (page: puppeteer.Page) => {
    let pageNumber: number = 0
    let totalUrls: any = {}
    await page.goto(`https://tradings.car1.hk/car-selection/-/-/?q=&page=${pageNumber}`)
    const searchBrand = await page.$$('#search-brand > option')
    const promisBrands = (await searchBrand)!.reduce(async (result, data) => {
        return (await result).concat(await ((await data.getProperty('value')).jsonValue()))
    }, Promise.resolve([]))
    const brands = (await promisBrands).filter((brand) => /*brand == "奧迪" || brand == "平治" || brand == "寶馬" || brand == "本田" || */brand == "保時捷" /*|| brand == "豐田"*/)
    // const brands=[brandTs[2]]
    console.log('allBrands' + brands)
    for (let brand of brands) {
        await page.goto(`https://tradings.car1.hk/car-selection/-/-/?q=${brand}&page=0`)
        let notFound = await page.$('#search-result > div > div > p')
        const current: any = []
        while (notFound == null) {
            console.log(`Scraping ${brand} ! Scrap Page ${pageNumber}`)
            const datas = await page.$$('ul#search-result > li > div.right > h2  a')
            if (datas != null) {
                const promiseUrls = (await datas).reduce(async (result, data) => {
                    return (await result).concat(await (await (data.getProperty('href'))).jsonValue())
                }, Promise.resolve([]))
                for (let url of await promiseUrls) {
                    current.push(url)
                }
            }
            pageNumber++
            await page.goto(`https://tradings.car1.hk/car-selection/-/-/?q=${brand}&page=${pageNumber}`)
            notFound = await page.$('#search-result > div > div > p')
        }
        pageNumber = 0
        totalUrls[brand] = current
        console.log(totalUrls)
    }
    return totalUrls
}

const brands = {
    //tradings.car1.hk/item/6687/Audi-A3-14T-FSI-2013/
    "奧迪": [
        "https://tradings.car1.hk/item/6687/Audi-A3-14T-FSI-2013/",
        "https://tradings.car1.hk/item/7026/Audi-A6-2009/",
        "https://tradings.car1.hk/item/7025/Audi-A6-28-Quatro-2009/",
        "https://tradings.car1.hk/item/7001/Audi-Q5-20T-Quattro-2014/"],
    "寶馬": [
        "https://tradings.car1.hk/item/7033/BMW-220i-Coupe-Sport-2014/",
        "https://tradings.car1.hk/item/6966/BMW-523iA-Touring-2010/",
        "https://tradings.car1.hk/item/6986/BMW-X1-2014/",
        "https://tradings.car1.hk/item/5786/BMW-E46-318-1999/"]
}

const singleWebScrap = async (brand: string, page: puppeteer.Page, url: string) => {
    console.log('sigleWebScrap start!')
    let result:car1 = {
        name: '',//string
        Year: null,//int|null
        isAtuo: true,//boolean
        seat: 0,//int|null
        Volume: 0,//int|null
        Detail: '',//string
        website: '',//string
        picture1: '',//string
        picture2: '',
        picture3: '',
        picture4: '',
        price: 0.,//int|null
        km: null,//int
        brand: '',//string
        isSold: false,//boolean
        url: '',//string,
        update: null,
        contactName: '',
        contactNo: null
    }
    await page.goto(url)
    const datas = await page.$$eval('div.details.clear > div.line ', es => {
        let aCarData = {}
        for (let e of es) {
            if (e.querySelector('.values') == null) {
                aCarData[e.querySelector('.fields')!.innerHTML.replace(/[\：\n\t\s]/g, '')] = e.innerHTML.replace(/([\t\n])/g, '')
            } else {
                aCarData[e.querySelector('.fields')!.innerHTML.replace(/[\：\n\t\s]/g, '')] = e.querySelector('.values')!.innerHTML.replace(/([\t\n])/g, '')
            }
        }
        return aCarData
    })
    console.log(datas)
    if (await page.$('div.trading_details.clear > div.right > h1  ')) {
        result.name = await page.$eval('div.trading_details.clear > div.right > h1  ', (e) => {
            return e.innerHTML.replace(/[\：\n\t\s]|(<s>)|<.s>|(已售)|[()]/g, '')
        })
    }
    result.website = 'https://www.car1.hk/'
    const pictures = await page.$$('#thumbs > li > a')
    const promisePictureUrls = (await pictures).reduce(async (result, picture) => {
        return (await result).concat(await (await (picture.getProperty('href'))).jsonValue())
    }, Promise.resolve([]))
    const pictureUrls = await promisePictureUrls
    if (pictureUrls != null) {
        result.picture1 = pictureUrls[0] ? pictureUrls[0] : null
        result.picture2 = pictureUrls[1] ? pictureUrls[1] : null
        result.picture3 = pictureUrls[2] ? pictureUrls[2] : null
        result.picture4 = pictureUrls[3] ? pictureUrls[3] : null
    }
    if (datas['製造年份']) {
        result.Year = parseInt(datas['製造年份'])
    }
    if (datas['汽缸容量']) {
        result.Volume = parseInt(datas['汽缸容量'].match(/[0-9]*/g)[0])
    }
    if (datas['坐位']) {
        result.seat = parseInt(datas['坐位'])
    }
    if (datas['價錢']) {
        const price = /\$[0-9,]*/g
        result.price = parseInt(datas['價錢'].match(price).pop().replace(/[$,]/g, ''))
    } else {
        result.price = null
    }
    if (datas['波箱']) {
        result.isAtuo = datas['波箱'] == '自動波' ? true : false
    }
    if (datas['現況']) {
        result.isSold = datas['現況'] == '已售' ? true : false
    }
    result.update = await page.$eval('#lhs_content > div.trading_details.clear > div.left > div > div:nth-child(2)', (e) => {
        return Date.parse(e.innerHTML.match(/([0-9][0-9])\/([0-9][0-9])\/([0-9][0-9])/g)![0])
    })
    if (datas['補充資訊']) {
        const additonal = /(<div class="fields">)|(<.div>)|(<div>)|(<br>)|-|(補充資訊：)/g
        result.Detail = datas['補充資訊'].replace(additonal, '')
    } else {
        result.Detail = null
    }
    if (datas['咪表讀數']) {
        result.km = parseInt(datas['咪表讀數'].match(/[0-9,]*/)[0].replace(/[$,]/g, ''))
    }
    const update = await page.$eval('#lhs_content > div.trading_details.clear > div.left > div > div:nth-child(2)', (e) => {
        return e.innerHTML.match(/([0-9][0-9])\/([0-9][0-9])\/([0-9][0-9])/g)![0].match(/([0-9][0-9])/g)
    })
    result.update = Date.parse(`20${update![2]}-${update![1]}-${update![0]}`)
    const contact = await page.$$eval('#lhs_content > div.trading_details.clear > div.right > div.line2.clear >div.line', es => {
        let contactData = {}
        for (let e of es) {
            if (e.querySelector('.values') == null) {
                contactData[e.querySelector('.fields')!.innerHTML.replace(/[\：\n\t\s]/g, '')] = ''
            } else {
                contactData[e.querySelector('.fields')!.innerHTML.replace(/[\：\n\t\s]/g, '')] = e.querySelector('.values')!.innerHTML.replace(/([\t\n])/g, '')
            }
        }
        return contactData
    })
    if (contact['聯絡']) {
        result.contactName = contact['聯絡']
    }
    if (contact['電話']) {
        if (contact['電話'].replace(/\s/, '').match(/(\d\d\d\d\d\d\d\d)/) != null) {
            result.contactNo = parseInt(contact['電話'].replace(/\s/, '').match(/(\d\d\d\d\d\d\d\d)/)[0])
        }
    }
    result.url = url
    result.brand = brand == "奧迪" ? 'Audi' : brand == "平治" ? 'MercedesBenz' : brand == "寶馬" ? "BMW" : brand == "本田" ? 'Honda' : brand == "保時捷" ? 'porsche' : "Toyota"
    console.log(result)
    return result
}



(async () => {
    const startTime = new Date().getTime()
    const brower = await puppeteer.launch({
        headless: false,
    })
    const result = []
    const page = await brower.newPage()
    console.log('Scrap Urls Finish ! Scrap single page now !')
    const brandsUrl = await scrapAllUrls(page)
    for (let brand in brandsUrl) {
        for (let url of brandsUrl[brand]) {
            result.push(await singleWebScrap(brand, page, url))
        }
        await jsonFile.writeFile('./result1.json', result, { spaces: 2 })
    }
    console.log(result)
    console.log('finish Scrap car1.hk')
    await brower.close()
    const runTime = new Date().getTime() - startTime
    console.log('runtime : ' + moment(runTime).format('mm分:ss秒:SS毫秒'))
    return result
})()



// const singleWebScrap1 = async () => {
//     console.log('sigleWebScrap start!')
//     const brower = await puppeteer.launch({
//         headless: false,
//     })
//     const page = await brower.newPage()
//     await page.goto("https://tradings.car1.hk/item/6641/Audi-RS5-2010/")
//     let result/*: car1Detail*/ = {
//         name: '',//string
//         Year: null,//int|null
//         isAtuto: true,//boolean
//         seat: 0,//int|null
//         Volume: 0,//int|null
//         Detail: '',//string
//         website: '',//string
//         picture1: '',//string
//         picture2: '',
//         picture3: '',
//         picture4: '',
//         price: 0.,//int|null
//         km: null,//int
//         brand: '',//string
//         isSold: false,//boolean
//         url: '',//string,
//         update: null,
//         contactName: '',
//         contactNo: 0
//     }
//     const datas = await page.$$eval('div.details.clear > div.line ', es => {
//         let aCarData = {}
//         for (let e of es) {
//             if (e.querySelector('.values') == null) {
//                 aCarData[e.querySelector('.fields')!.innerHTML.replace(/[\：\n\t\s]/g, '')] = e.innerHTML.replace(/([\t\n])/g, '')
//             } else {
//                 aCarData[e.querySelector('.fields')!.innerHTML.replace(/[\：\n\t\s]/g, '')] = e.querySelector('.values')!.innerHTML.replace(/([\t\n])/g, '')
//             }
//         }
//         return aCarData
//     })
//     console.log(datas)
//     if (await page.$('div.trading_details.clear > div.right > h1  ')) {
//         result.name = await page.$eval('div.trading_details.clear > div.right > h1  ', (e) => {
//             return e.innerHTML.replace(/[\：\n\t\s]|(<s>)|<.s>|(已售)|[()]/g, '')
//         })
//     }
//     result.website = 'https://www.car1.hk/'
//     const pictures = await page.$$('#thumbs > li > a')
//     const promisePictureUrls = (await pictures).reduce(async (result, picture) => {
//         return (await result).concat(await (await (picture.getProperty('href'))).jsonValue())
//     }, Promise.resolve([]))
//     const pictureUrls = await promisePictureUrls
//     if (pictureUrls != null) {
//         result.picture1 = pictureUrls[0] ? pictureUrls[0] : null
//         result.picture2 = pictureUrls[1] ? pictureUrls[1] : null
//         result.picture3 = pictureUrls[2] ? pictureUrls[2] : null
//         result.picture4 = pictureUrls[3] ? pictureUrls[3] : null
//     }
//     if (datas['製造年份']) {
//         result.Year = parseInt(datas['製造年份'])
//     }
//     if (datas['汽缸容量']) {
//         result.Volume = parseInt(datas['汽缸容量'].match(/[0-9]*/g)[0])
//     }
//     if (datas['坐位']) {
//         result.seat = parseInt(datas['坐位'])
//     }
//     if (datas['價錢']) {
//         const price = /\$[0-9,]*/g
//         result.price = parseInt(datas['價錢'].match(price).pop().replace(/[$,]/g, ''))
//     } else {
//         result.price = null
//     }
//     if (datas['波箱']) {
//         result.isAtuto = datas['波箱'] == '自動波' ? true : false
//     }
//     if (datas['現況']) {
//         result.isSold = datas['現況'] == '已售' ? true : false
//     }

//     if (datas['補充資訊']) {
//         const additonal = /(<div class="fields">)|(<.div>)|(<div>)|(<br>)|-|(補充資訊：)/g
//         result.Detail = datas['補充資訊'].replace(additonal, '')
//     } else {
//         result.Detail = null
//     }
//     if (datas['咪表讀數']) {
//         result.km = parseInt(datas['咪表讀數'].match(/[0-9,]*/)[0].replace(/[$,]/g, ''))
//     }
//     const update = await page.$eval('#lhs_content > div.trading_details.clear > div.left > div > div:nth-child(2)', (e) => {
//         return e.innerHTML.match(/([0-9][0-9])\/([0-9][0-9])\/([0-9][0-9])/g)[0].match(/([0-9][0-9])/g)
//     })
//     result.update = Date.parse(`20${update[2]}-${update[1]}-${update[0]}`)
//     const contact = await page.$$eval('#lhs_content > div.trading_details.clear > div.right > div.line2.clear >div.line', es => {
//         let contactData = {}
//         for (let e of es) {
//             if (e.querySelector('.values') == null) {
//                 contactData[e.querySelector('.fields')!.innerHTML.replace(/[\：\n\t\s]/g, '')] = ''
//             } else {
//                 contactData[e.querySelector('.fields')!.innerHTML.replace(/[\：\n\t\s]/g, '')] = e.querySelector('.values')!.innerHTML.replace(/([\t\n])/g, '')
//             }
//         }
//         return contactData
//     })
//     if (contact['聯絡']) {
//         result.contactName = contact['聯絡']
//     }
//     if (contact['電話']) {
//         if (contact['電話'].replace(/\s/, '').match(/(\d\d\d\d\d\d\d\d)/) != null) {
//             result.contactNo = parseInt(contact['電話'].replace(/\s/, '').match(/(\d\d\d\d\d\d\d\d)/)[0])
//         }
//     }

//     console.log(result)
//     return result
// }
// singleWebScrap1()
// singleWebScrap1()
// singleWebScrap()
// //             if (datas != null) {
// //                 const promiseUrls = (await datas).reduce(async (result, data) => {
// //                     return (await result).concat(await (await (data.getProperty('href'))).jsonValue())
// //                 }, Promise.resolve([]))
// //                 for (let url of await promiseUrls) {
// //                     current.push(url)
// //                 }
// //             }
