//import * as Knex from 'knex'
import * as puppeteer from 'puppeteer'
import * as fs from 'fs';

require('fs').promises

//const knexFile = require('./knexfile');
//const knex = Knex(knexFile[process.env.NODE_ENV || 'development']);

export class ScrapService {
    constructor() {

    }
    scrapPriceCom = async (pages: number, filter:string, file:string): Promise<any[]> => {
        const browser = await puppeteer.launch({
            headless: false,
        });
        const page = await browser.newPage();
        
        let carArr:object[] = []

        let filterObj = {
            Toyota: 268,
            Honda:237,
            Porshe:260,
            Audi:225,
            BMW:227,
            Benz:251
        }

        for (let i = 1; i <= pages; i++) {
            let URL = `https://car.price.com.hk/product/list?ptype=1&page=${i}&filter=${filterObj[filter]},289`
            await page.goto(URL);
            await page.waitFor('.wrapper-product')
           
             //console.log(await page.$$('.wrapper-product'))
            // const products = await page.$$('.wrapper-product');
            
            if (await page.$$('.wrapper-product') !== null) {
                // let Words =

                const jakes = await page.evaluate((carArr) => {
                    
                    //const brand = Array.from(document.querySelectorAll('.wrapper-filter'))
                    //brand.map((elem: any) => {
                        //jake['brand'] = elem.querySelector('.filter-text')!.innerText
                    //});
                    
                    let items = Array.from(document.querySelectorAll(".wrapper-product"));
                    //console.log("items", items) 
                    return items.map((elem: any) => {
                        let jake = {};
                        
                           const brand = Array.from(document.querySelectorAll('.wrapper-filter'))
                           let brandd = brand.map((e:any)=>{
                            return e.querySelector('.filter-text')!.innerText
                           })
                               
                        try {
                            jake['brand'] = brandd[0].replace(/[^a-zA-Z]/g,'')
                            jake['name'] = elem.querySelector('.product-name')!.innerText
                            jake['year'] = elem.querySelector('.product-attribute span:nth-child(1)')!.innerText.replace(/[^0-9]/g,'')
                            jake['seats'] = elem.querySelector('.product-attribute span:nth-child(3)')!.innerText.replace(/[^0-9]/g,'')
                            jake['volume'] = elem.querySelector('.product-attribute span:nth-child(7)')!.innerText.replace(/[^0-9]/g,'')
                            jake['isAuto'] = elem.querySelector('.product-attribute span:nth-child(9)')!.innerText.includes("AT") ? true : false
                            jake['detail'] = elem.querySelector('.product-description')!.innerText
                            jake['contactName'] = elem.querySelector('.wrapper.wrapper-product-contact > .product-contact-content:nth-child(2)')!.innerText
                            jake['contactNo'] = elem.querySelector('.wrapper-product-contact > .product-contact-content:nth-child(4)')!.innerText.replace(/[^0-9]/g,'')
                            jake['price'] = elem.querySelector('.product-price span:nth-child(2)')!.innerText.replace(/[^0-9]/g,'')
                            jake['updateDate'] = elem.querySelector('.wrapper-product-update-date span:nth-child(2)')!.innerText
                            jake['imgLink'] = elem.querySelector('.product-image').getAttribute('style').slice(23, 86)
                            jake['websiteName'] = 'https://car.price.com.hk'
                            jake['url'] = elem.querySelector('.wrapper-product-image > a').getAttribute('href')
                            jake['isSold'] = false;
                            jake['year'] = parseInt(jake['year'],10)
                            jake['seats'] = parseInt(jake['seats'],10)
                            jake['volume'] = parseInt(jake['volume'],10)
                            jake['contactNo'] = parseInt(jake['contactNo'],10)
                            jake['price'] = parseInt(jake['price'],10)
                            return jake;
                            //carArr
                            // Words;
                        } catch (e) {
                            console.log(e);
                            return jake;
                        }
                    })
                    
                    
                })   
                
                jakes.map((e:object)=>{
                    return carArr.push(e);
                })
            
            } else {
                continue;
            }
        }
        //        let item = await Words.map(async (url)=>{
        //            await page.goto(url);
        //            let content = await page.$('body > section > section > div.wrapper.wrapper-main > main > div.wrapper.wrapper-detail-panel.wrapper-product-detail');
        //            return await Array.from(content);
        //        })
        console.log('scrapped!', carArr);
        await browser.close();
        await fs.promises.writeFile(file, JSON.stringify(carArr));
        return carArr;
    }

//    scrapeInsert = async (data:object[]) => {
//        const hasTable = await this.knex.schema.hasTable('carInfo')
//        if (hasTable) {
//            try{
//            await this.knex.batchInsert('carInfo', data, 10)
//            return data;
//            }catch(e) {
//                console.log(e);
//                return e;
//            }
//        }
//    }
}

let New = new ScrapService();
//New.scrapPriceCom(39, "Honda",'../../json/price.com.seed/Honda.json');
//New.scrapPriceCom(50, "Toyota", '../../json/price.com.seed/Toyota.json');
//New.scrapPriceCom(25, "Porshe",'../../json/price.com.seed/Porshe.json')
//New.scrapPriceCom(40, "Audi", '../../json/price.com.seed/Audi.json');
//New.scrapPriceCom(50, "BMW", '../../json/price.com.seed/BMW.json');
//New.scrapPriceCom(50, "Benz", '../../json/price.com.seed/Benz.json');
