import axios from 'axios';
import * as puppeteer from 'puppeteer';
import * as jsonFile from 'jsonfile';
import { dchuu } from '../../model/model';

(async () => {
    const api = await axios.get('https://www.dchucc.com/api//cars-web.php?lang=tc')
    const db: dchuu[] = []
    const brower = await puppeteer.launch({
        headless: false,
    })
    const page = await brower.newPage()
    for (let data of api.data) {
        let detial: dchuu= {
            name: '',//string
            Year: 0,//int|null
            isAtuto: true,//boolean
            seat: 0,//int|null
            Volume: 0,//int|null
            Detail: '',//string
            website: '',//string
            picture1: '',//string
            picture2: '',
            picture3: '',
            picture4: '',
            price: null,//int|null
            km: null,//int
            brand: '',//string
            isSold: false,//boolean
            url: '',//string,
        }
        await page.goto(`https://www.dchucc.com/tc/#detail/${data.id}`)
        await page.waitFor(3000)
        detial.brand = data.brand
        detial.Year = parseInt(data.yearOfManufacture)
        detial.Volume = parseInt(data.engineDisplacement)
        detial.isAtuto = data.transmission == '自動波' ? true : false
        detial.name = data.model
        detial.picture1 = 'https://www.dchucc.com/api/thumb.php?image=' + data.picture1
        detial.picture2 = 'https://www.dchucc.com/api/thumb.php?image=' + data.picture2
        detial.picture3 = 'https://www.dchucc.com/api/thumb.php?image=' + data.picture3
        detial.seat = data.seatingCapacity ? parseInt(data.seatingCapacity) : null
        detial.price = parseInt(data.priceHK)
        detial.website = 'https://www.dchucc.com'
        detial.url = `https://www.dchucc.com/tc/#detail/${data.id}`
        const pageDetail = await page.$$eval('#car-detail-placeholder > div.search-inner > div.row-fluid.banner-set.banner-set-detail > div.span4.no-margin > div.banner-bg > div.banner-detail > ul > li', (es) => {
            const result = {}
            for (let e of es) {
                result[e.querySelector('.span4')!.innerHTML.replace(/[\s:]/g, '')] = e.querySelector('.span5')!.innerHTML.replace(/[\s:]/g, '')
            }
            return result
        })
        if (pageDetail['行車里數(km)']) {
            detial.km = parseInt(pageDetail['行車里數(km)'].replace(/[km,]/g, ''))
        }
        if (pageDetail['主要設備']) {
            detial.Detail = pageDetail['主要設備']
        }
        db.push(detial)
    }
    // console.log(pageDetail)
    await jsonFile.writeFile('./result3.json', db, { spaces: 2 })
    console.log('finish')
    await brower.close()
})()
