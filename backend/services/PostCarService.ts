import * as knex from 'knex';
import { postcarBody, carData } from '../model/model';
import { ToElasticQueryData, queryNameMatch, queryBrandMatch } from '../model/model';
import { carModel } from '../model/carModel';
import axios from 'axios';

export class PostCarService {
    constructor(private knex: knex) {

    }

    postCar = async (postData: postcarBody, location?: string) => {
        console.log('hah')
        const brandTable = await this.knex.select('id', 'brandName').from('brand')
        console.log(brandTable)
        const websitesId = await this.knex.select('id', "websiteName").from('website').where('websiteName', 'carlotb').first()
        console.log(websitesId)
        const user = await this.knex.select('id', 'username').from('users').where('username', postData.username).first()
        console.log(user)
        const brands = {}
        for (let brand of brandTable) {
            brands[brand.brandName] = brand.id
        }


        const result = {
            brand_id: +(brands[postData.brand]),
            name: postData.carname,
            year: +(postData.year),
            isAuto: postData.isAuto == 'AT' ? true : false,
            seats: +(postData.seats),
            volume: +(postData.volume),
            detail: postData.detail,
            website_id: +(websitesId.id),
            price: +(postData.price),
            imgLink: location ? location : null,
            contactName: postData.contactName,
            contactNo: postData.contactNo,
            isSold: false,
            km: +(postData.km),
            updateDate: new Date(),
            user_id: +(user.id),
            rental: postData.rental == 'Yes' ? true : false
        }

        this.knex.transaction(async (trx: knex.Transaction) => {
            try {
                const carInfoId = await trx("carInfo").insert(result).returning('id')
                if (postData.rental == 'Yes') {
                    await trx('rental').insert({
                        User_id: user.id,
                        days: postData.rental_day == '' || postData.rentalPrice == undefined ? null : parseInt(postData.rental_day),
                        RentalPrice: postData.rentalPrice == '' || postData.rentalPrice == undefined ? null : parseInt(postData.rentalPrice),
                        car_id: carInfoId[0],
                        isPay: false,
                    }).return('id')
                }

                const carDatas: carData = await trx.select("carInfo.id", "carInfo.name", "year", "isAuto", "seats", "volume", "detail", "price", "url", "contactName", "contactNo", "isSold", "km", "brandName", "websiteName", "imgLink", "updateDate", "user_id").from("carInfo")
                .leftJoin('brand', 'carInfo.brand_id', 'brand.id')
                .join("website", "carInfo.website_id", 'website.id')
                .where("carInfo.id", carInfoId[0]).first()
                const postToElastic = await axios.post(`${process.env.elasticSearchService}/car/doc/${carInfoId[0]}`, carDatas)
                console.log(postToElastic)
                console.log(carInfoId)
                return { 'msg': 'success' }
            } catch (error) {
                console.log(error)
                return error
            }
        })

    }
    
    loadpriceSingle = async (brand: string) => {
        const datas = {}
        const searchCondition: ToElasticQueryData = { query: { bool: { must: [null] } }, size: 10000 }
        const brands: queryBrandMatch = { match: { brandName: brand } }
        searchCondition.query.bool.must.push(brands)

        for (let i = 0; i < carModel[brand].length; i++) {
            if (i != 0) {
                searchCondition.query.bool.must.pop()
            }
            const models: queryNameMatch = { match: { name: carModel[brand][i] } }
            searchCondition.query.bool.must.push(models)
            console.log(JSON.stringify(searchCondition))
            const res = await axios({
                method: "GET",
                url: `${process.env.elasticSearchService}/car/_search/`,
                data: searchCondition,
                headers: { "Authorization": `${process.env.elasticSearchLogin}` }
            })
            if (res.data.hits.total != 0) {
                datas[carModel[brand][i]] = { datas: res.data.hits.hits, amount: res.data.hits.total.value, label: [], prices: [] }
            }
        }
        for (let key in datas) {
            console.log(datas[key])
            for (let data of datas[key]["datas"]) {
                // console.log(data)
                datas[key].label.push(data["_source"]["year"]),
                    datas[key].prices.push(data["_source"]["price"])
            }
        }
        return datas
    }
    loadpriceDouble = async (brand: string, name: string) => {
        console.log('double')
        const searchCondition: ToElasticQueryData = { query: { bool: { must: [null] } }, size: 10000 }
        const brands: queryBrandMatch = { match: { brandName: brand } }
        searchCondition.query.bool.must.push(brands)
        const queryNameMatch: queryNameMatch = { match: { name: name } }
        searchCondition.query.bool.must.push(queryNameMatch)
        console.log(searchCondition)
        const res = await axios({
            method: "GET",
            url: `${process.env.elasticSearchService}/car/_search/`,
            data: searchCondition,
            headers: { "Authorization": `${process.env.elasticSearchLogin}` }
        })
        const data = res.data.hits.hits

        data.sort((a: number, b: number) => { return a["_source"]["year"] - b["_source"]['year'] })

        let datas: { price: string[] | null[], year: string[] | null[][], data: string[] | null[] } = { price: [], year: [], data: [] }
        datas.data = data
        for (let one of data) {
            datas.price.push(one["_source"]["price"])
            datas.year.push(one["_source"]["year"])
        }
        return datas
    }
}