import * as express from 'express';
import { PostCarService } from '../services/PostCarService';
import * as multer from 'multer';





export class PostCarRouter {
    constructor(private postcarservice: PostCarService, private upload: multer.Instance) {
    }



    router = () => {
        const router = express.Router()
        router.post('/car', this.upload.single('file'), this.postCar)
        router.post('/pricecheck', this.checkPrice)
        return router
    }


    postCar = async (req: express.Request, res: express.Response) => {
        try {

            // const {
            //     username,
            //     brand,
            //     carname,
            //     year,
            //     isAuto,
            //     seats,
            //     volume,
            //     detail,
            //     price,
            //     contactName,
            //     contactNo,
            //     km,
            //     rental,
            //     rentalPrice,
            //     rental_day
            // } = req.body as postcarBody;
            
            console.log(req.body)
            const post = await this.postcarservice.postCar(req.body, (req.file as any).location)
            res.json(post)
        } catch (error) {
            res.json(error)
        }
    }

    



    checkPrice = async (req: express.Request, res: express.Response) => {
        const { carname, brand } = req.body
        console.log(carname, brand)
        if (carname == '') {
            const data = await this.postcarservice.loadpriceSingle(brand)
            res.json(data).status(200)
        }
        if (carname != '' && brand != '') {
            const data = await this.postcarservice.loadpriceDouble(brand, carname)
            res.json(data).status(200)
        }
    }
}
