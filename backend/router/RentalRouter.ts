import * as express from 'express';
import { RentalService } from '../services/RentalService';
import {  isLoggedIn } from '../app'
export class RentalRouter {
    constructor(private rentalservice: RentalService) {

    }

    router() {
        const router = express.Router()
        router.get('/car', this.getCar)
        router.post('/pay', isLoggedIn, this.payment)
        return router
    }

    getCar = async (req: express.Request, res: express.Response) => {
        try {
            res.json(await this.rentalservice.getRentalCar())
        } catch (error) {
            res.json(error)
        }
    }

    payment = async (req: express.Request, res: express.Response) => {
        try {
            const { product, token } = req.body
            await this.rentalservice.payment(product, token)
            res.json({ "msg": 'Payment Success!' }).status(200)
        } catch (error) {
            res.status(200).json({ "msg": error.message })
        }
    }

}