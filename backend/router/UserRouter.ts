import * as express from 'express'
import { UserService } from '../services/UserService';

export class UserRouter {

    constructor(private userservice: UserService) {
    }
    router = () => {
        const router = express.Router()
        router.post('/localuser',  this.createLocalUser)
        router.get('/:username', this.getUserByUsername)
        // router.post('/addfav', this.addToFavorites)
        return router;
    }

    createLocalUser = async (req: express.Request, res: express.Response) => {
        try {
            res.status(200).json(await this.userservice.createUserByLocal(req.body.username, req.body.password))
        } catch (error) {
            res.status(401).json(error.message)
        }
    }
    getUserByUsername = async (req: express.Request, res: express.Response) => {
        try {
            res.json(await this.userservice.getUserByUsername(req.params.username))
        } catch (error) {
            res.json(error.message)
        }
    }

    // addToFavorites = async (req: express.Request, res: express.Response) => {
    //     try {
    //         res.json(await this.userservice.addFavortiesPost(req.body.username,req.body.car_id))
    //     } catch (error) {
    //         res.json(error.message)
    //     }
    // }
    

}