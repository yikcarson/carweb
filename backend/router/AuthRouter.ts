import * as express from 'express'
import jwt from '../jwt'
import { UserService } from '../services/UserService';
import * as jwtSimple from 'jwt-simple';
import axios from 'axios'

export class AuthRouter {
    constructor(private usersevice: UserService) {

    }
    router = () => {
        const router = express.Router()
        router.post('/local', this.login)
        router.post('/facebook', this.loginFacebook)
        return router
    }

    login = async (req: express.Request, res: express.Response) => {
        console.log(req.body)
        if (!req.body.username || !req.body.password) {
            res.status(401).json({ msg: 'Wrong username or password' })
            return
        }
        const { username, password } = req.body
        const user = await this.usersevice.checkUserPassword(username, password)
        if (!user) {
            res.status(401).json({ msg: 'Wrong username or password' })
            return
        }
        //Generate JWT
        const payload = {
            id: user.id,
            username: user.username
        }
        const token = jwtSimple.encode(payload, jwt.jwtSecret)
        res.json({ token, username })
    }

    loginFacebook = async (req: express.Request, res: express.Response) => {
        if (!req.body.accessToken) {
            res.status(401).json({ msg: "Wrong Access Token!" });
            return;
        }
        const { accessToken } = req.body;
        try {
            const result = await axios.get(`https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`);
            // console.log(await this.usersevice.getUserByUsername(result.data.email))
            // console.log(await this.usersevice.getUserByFacebookId(result.data.id))
            let user = (await this.usersevice.getUserByFacebookId(result.data.id)) || (await this.usersevice.getUserByUsername(result.data.email))
            console.log(user)
            if (!user) {
                console.log('create')
                console.log(user)
                user = await this.usersevice.createUserByFacebook(result.data.email, result.data.id);
            }
            const payload = {
                id: user.id,
                username: user.username
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            res.status(200).json({
                token,
                username: user.username
            });
        } catch (e) {
            console.error(e);
            res.status(401).json({ msg: "Wrong Access Token!" });
            return;
        }
    }
}