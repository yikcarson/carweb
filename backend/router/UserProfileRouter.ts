import * as express from 'express';
import { UserService } from '../services/UserService';

export class UserProfileRouter {
    constructor(private userservice: UserService) {

    }
    router() {
        const router = express.Router()
        router.get('/post/:username', this.getPostCarbyUsername)
        router.get('/fav/:username', this.getPostToFav)
        router.get('/rental/:username', this.getRentalcarRecond)
        router.post('/fav', this.addPostToFav)
        router.delete('/fav', this.delPostToFav)
        return router
    }

    getPostCarbyUsername = async (req: express.Request, res: express.Response) => {
        try {
            res.json(await this.userservice.getPostCarbyUsername(req.params.username))
        } catch (error) {
            return error
        }
    }
    addPostToFav = async (req: express.Request, res: express.Response) => {
        try {
            res.json(await this.userservice.addFavortiesPost(req.body.car_id, req.body.username))
        } catch (error) {
            res.json(error)
        }
    }
    delPostToFav = async (req: express.Request, res: express.Response) => {
        try {
            let result = res.json(await this.userservice.deletFavortiesPost(req.body.car_id, req.body.username))
            console.log(result);
        } catch (error) {
            res.json(error)
        }
    }

    getPostToFav = async (req: express.Request, res: express.Response) => {
        try {
            res.json(await this.userservice.getFavortiesPost(req.params.username))
        } catch (error) {
            res.json(error)
        }
    }
    getRentalcarRecond = async (req: express.Request, res: express.Response) => {
        try {
            res.json(await this.userservice.getRentalRecond(req.params.username))

        } catch (error) {
            res.json(error)
        }
    }
}





