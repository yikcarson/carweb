import * as express from 'express'


export class RestoreLoginRouter {
    router() {
        const router = express.Router()
        router.get('/', this.getUsername)
        return router
    }

    getUsername = (req: express.Request, res: express.Response) => {
        res.json(req.user)
    }
}