import * as express from 'express'
import { ElasticSearchService } from '../services/ElasticSearchService'


export class ElasticSearchRouter {
    constructor(private elasticSearchService: ElasticSearchService) {

    }
    router() {
        const router = express.Router()
        // router.get('/load', this.load)
        router.post('/quick', this.quickSearch)
        // router.post('/customize', this.customizeSearch)
        router.get('/test',this.test)
        return router
    }

    private quickSearch = async (req: express.Request, res: express.Response) => {
        try {

            const result = await this.elasticSearchService.quickSearch(req.body.criteria)
            console.log(result)
            if (result) {
                res.status(200).json(result)
            }
        } catch (error) {
            res.status(400).json('gg')
        }
    }
    private test = async (req: express.Request, res: express.Response) => {
        // const result =await this.elasticSearchService.createJson()
        const result =await this.elasticSearchService.createJson()
        res.json(result)
    }
}