import * as  express from 'express'
import * as  bodyParser from 'body-parser'
import { UserService } from './services/UserService';
import * as Knex from 'knex'
import * as cors from 'cors'
import { initializePassport } from './passport';
import { AuthRouter, ElasticSearchRouter,PostCarRouter,RentalRouter,RestoreLoginRouter,UserProfileRouter,UserRouter } from './router';
import * as passport from 'passport';
import { ElasticSearchService } from './services/ElasticSearchService';
import { PostCarService } from './services/PostCarService';
import { upload } from './services/S3';
import { RentalService } from './services/RentalService';
// import * as Stripe from 'stripe'



const app = express()

const knexConfig = require('./knexfile')
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development'])
const Port = 8080 || process.env.port
// const stripe= new Stripe(`${process.env.stripeKey}`)
//————————App.Use————————————————————————————————————————————————————————————————
app.use(bodyParser.json())
app.use(cors())
app.use((req, res, next) => {
    const date = new Date().toLocaleString('zh');
    console.log(`[${date}] Request ${req.url}`);
    next();
})
//———————————————————————————————————————————————————————————————————————————————
app.get('/',(req,res)=>{
    res.json('api.carlotb.com')
})


//——————————————Service——————————————————————————————————————————————————————————
const userService = new UserService(knex)
const elasticsearchService = new ElasticSearchService(knex)
const postcarservice=new PostCarService(knex)
const rentalservice=new RentalService(knex)
//———————————————————————————————————————————————————————————————————————————————
initializePassport(userService);




export const isLoggedIn = passport.authenticate('jwt', { session: false });
//——————————————Router——————————————————————————————————————————————————————————
const userRouter = new UserRouter(userService)
const authRouter = new AuthRouter(userService)
const elasticsearchRouter = new ElasticSearchRouter(elasticsearchService)
const restoreoginRouter = new RestoreLoginRouter()
const postcarRouter=new PostCarRouter(postcarservice,upload)
const userprofileRouter = new UserProfileRouter(userService)
const rentalRouter=new RentalRouter(rentalservice)
//——————————————————————————————————————————————————————————————————————————————



//——————————————Router Path——————————————————————————————————————————————————————
app.use('/user', userRouter.router())
app.use('/login', authRouter.router())
app.use('/restoreLogin', isLoggedIn, restoreoginRouter.router())
app.use('/search',elasticsearchRouter.router())
app.use('/postcar',isLoggedIn,postcarRouter.router())
app.use('/rental',rentalRouter.router())
app.use('/profile',isLoggedIn,userprofileRouter.router())
//———————————————————————————————————————————————————————————————————————————————




app.listen(Port, () => {
    console.log(`${Port} is start ！`)
})



