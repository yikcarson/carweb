import * as Knex from "knex";
import * as jsonfile from 'jsonfile'

export async function seed(knex: Knex): Promise<any> {
    // Deletes ALL existing entries
    try {

        await knex.transaction(async (trx) => {
            // await trx("rental").del();

            await trx('carInfo').del();
            await trx('brand').del();
            await trx('website').del();
            await trx('picture').del();

            //call all jsonfiles
            const priceFile = await jsonfile.readFile('./json/price.com.seed/price.com.json');
            const car1 = await jsonfile.readFile('./json/car1.json');
            const dchuu = await jsonfile.readFile('./json/dchuu.json');

            //console.log(AudiFile[1]);

            //brand table
            const brand = await trx.batchInsert('brand', [
                { brandName: 'Audi' },
                { brandName: 'MercedesBenz' },
                { brandName: 'BMW' },
                { brandName: 'Honda' },
                { brandName: 'Porsche' },
                { brandName: 'Toyota' },
            ]).returning(['id', 'brandName']);
            //obj for foreign id brand
            let brandObj = {}
            for (const row of brand) {
                brandObj[row.brandName] = row.id
            }
            //website table
            const website = await trx.batchInsert('website', [
                { websiteName: 'price.com.hk', websiteLink: 'https://car.price.com.hk' },
                { websiteName: 'car1.hk', websiteLink: 'https://www.car1.hk' },
                { websiteName: '大昌行', websiteLink: 'https://www.dchucc.com' },
                { websiteName: "carlotb", websiteLink: "https://www.carlotb.com" }

            ]).returning(['id', 'websiteLink']);
            //obj for foreign id website
            let websiteObj = {}
            for (const row of website) {
                websiteObj[row.websiteLink] = row.id
            }
            // console.log(brandObj, websiteObj);

            // Price 
            const pricePromise = priceFile.map(async (e: object) => {
                const car = await trx.insert({
                    brand_id: brandObj[e['brand']],
                    name: e['name'],
                    year: e['year'],
                    seats: e['seats'],
                    volume: e['volume'],
                    isAuto: e['isAuto'],
                    detail: e['detail'],
                    contactName: e['contactName'],
                    contactNo: e['contactNo'],
                    price: e['price'],
                    updateDate: e['updateDate'],
                    imgLink: e['imgLink'],
                    website_id: websiteObj[e['websiteName']],
                    url: e['url'],
                    isSold: e['isSold'] || false
                }).into("carInfo").returning(["id"]);

                return { id: car[0].id, imgLink: e['imgLink'] || '' }
            })

            // Car1.hk
            const car1Promise = car1.map(async (e: object) => {
                const car = await trx.insert({
                    brand_id: brandObj[e['brand']],
                    name: e['name'],
                    year: e['Year'],
                    seats: e['seat'],
                    volume: e['Volume'],
                    isAuto: e['isAuto'],
                    detail: e['Detail'],
                    contactName: e['contactName'],
                    contactNo: e['contactNo'],
                    price: e['price'],
                    updateDate: e['update'],
                    imgLink: e['imgLink'],
                    km: e['km'],
                    website_id: websiteObj[e['websiteName']],
                    url: e['url'],
                    isSold: e['isSold']
                }).into('carInfo').returning(['id']);

                return { id: car[0].id, imgLink: e['imgLink'] || '' }
            })

            // DCHUU
            const dchuuPromise = dchuu.map(async (e: object) => {
                const car = await trx.insert({
                    brand_id: brandObj[e['brand']],
                    name: e['name'],
                    year: e['Year'],
                    seats: e['seat'] || 0,
                    volume: e['Volume'],
                    isAuto: e['isAuto'],
                    detail: e['Detail'],
                    contactName: e['contactName'],
                    contactNo: e['contactNo'],
                    price: e['price'],
                    updateDate: e['update'],
                    imgLink: e['imgLink'],
                    km: e['km'],
                    website_id: websiteObj[e['website']],
                    url: e['url'],
                    isSold: e['isSold']
                }).into('carInfo').returning(['id']);
                return { id: car[0].id, imgLink: e['imgLink'] || '' }
            })

            const pricePic = await Promise.all(pricePromise)
            const car1Pic = await Promise.all(car1Promise)
            const dchuuPic = await Promise.all(dchuuPromise)
            await trx.batchInsert('picture', [...pricePic, ...car1Pic, ...dchuuPic])
            // await trx.batchInsert('picture', car1Pic)
            // await trx.batchInsert('picture', dchuuPic)
            

            // await Promise.all([pricePic/*,resultCar1,resultDchuu */])
            // return await trx.commit()
            //await console.log('price.com finished!!!!!!!!!!!!!!!!!!!!!')
        })
    } catch (e) {
        console.error(e)
    } 
    // finally {
    //     await knex.destroy();
    // }
};
