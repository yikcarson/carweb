import * as Knex from "knex";

export async function seed(knex: Knex): Promise<any> {
    // Deletes ALL existing entries
    
    try{
    await knex("rental").del()
            // Inserts seed entries
           await knex.batchInsert('rental',[
                {
                    User_id: 1,
                    car_id: 6021,
                    start_date: "2019-07-11",
                    days: 1,
                    RentalPrice: 600,
                    isPay: false

                }, {
                    User_id: 1,
                    car_id: 1500,
                    start_date: "2019-07-12",
                    days: 1,
                    RentalPrice: 700,
                    isPay: false

                }, {
                    User_id: 1,
                    car_id: 2000,
                    start_date: "2019-07-14",
                    days: 1,
                    RentalPrice: 600,
                    isPay: false

                }, {
                    User_id: 1,
                    car_id: 2500,
                    start_date: "2019-07-20",
                    days: 1,
                    RentalPrice: 600,
                    isPay: false

                }, {
                    User_id: 1,
                    car_id: 3000,
                    start_date: "2019-07-15",
                    days: 1,
                    RentalPrice: 600,
                    isPay: false

                },{
                    User_id: 1,
                    car_id: 4200,
                    start_date: "2019-07-22",
                    days: 1,
                    RentalPrice: 850,
                    isPay: false

                },{
                    User_id: 1,
                    car_id: 2310,
                    start_date: "2019-07-15",
                    days: 1,
                    RentalPrice: 600,
                    isPay: false

                },{
                    User_id: 1,
                    car_id: 3900,
                    start_date: "2019-07-20",
                    days: 1,
                    RentalPrice: 700,
                    isPay: false

                },{
                    User_id: 1,
                    car_id: 3130,
                    start_date: "2019-07-22",
                    days: 1,
                    RentalPrice: 900,
                    isPay: false

                },{
                    User_id: 1,
                    car_id: 3250,
                    start_date: "2019-07-25",
                    days: 1,
                    RentalPrice: 550,
                    isPay: false

                },{
                    User_id: 1,
                    car_id: 4888,
                    start_date: "2019-07-20",
                    days: 1,
                    RentalPrice: 800,
                    isPay: false

                },
                {
                    User_id: 1,
                    car_id: 5322,
                    start_date: "2019-07-25",
                    days: 1,
                    RentalPrice: 550,
                    isPay: false

                },{
                    User_id: 1,
                    car_id: 5433,
                    start_date: "2019-07-29",
                    days: 1,
                    RentalPrice: 550,
                    isPay: false

                },{
                    User_id: 1,
                    car_id: 5544,
                    start_date: "2019-07-25",
                    days: 1,
                    RentalPrice: 550,
                    isPay: false

                }
            ]);
            await knex('carInfo').update({rental:true}).where('id', 6021)
            await knex('carInfo').update({rental:true}).where('id', 1500)
            await knex('carInfo').update({rental:true}).where('id', 2000)
            await knex('carInfo').update({rental:true}).where('id', 2500)
            await knex('carInfo').update({rental:true}).where('id', 3000)
            await knex('carInfo').update({rental:true}).where('id', 4200)
            await knex('carInfo').update({rental:true}).where('id', 2310)
            await knex('carInfo').update({rental:true}).where('id', 3900)
            await knex('carInfo').update({rental:true}).where('id', 3130)
            await knex('carInfo').update({rental:true}).where('id', 3250)
            await knex('carInfo').update({rental:true}).where('id', 4888)
            await knex('carInfo').update({rental:true}).where('id', 5322)
            await knex('carInfo').update({rental:true}).where('id', 5433)
            await knex('carInfo').update({rental:true}).where('id', 5544)
        } catch(e) {
            console.log(e);
        }
        // });
};
