import * as Knex from "knex";
import { hashPassword } from "../services/hash";

export async function seed(knex: Knex) {
    try {
        await knex("rental").del();

        await knex('users').where('username', 'admin').del()

        await knex('users').insert({
            username: 'admin',
            password: await hashPassword('admin')
        })
        return
    } catch (error) {
        return error
    }
}