// Update with your config settings.
import * as dotenv from 'dotenv'
dotenv.config();

module.exports = {

  development: {
    debug: true,
    client: "postgresql",
    connection: {
      database: process.env.DB_Name,
      user: process.env.DB_User,
      password: process.env.DB_Password
    },
    pool: {
      min: 2,
      max: 20
    },
    migrations: {
      tableName: "knex_migrations"
    }
  },

  staging: {
    client: "postgresql",
    connection: {
      database: "my_db",
      user: "username",
      password: "password"
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  },

  production: {
    client: "postgresql",
    connection: {
      database: "my_db",
      user: "username",
      password: "password"
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  },

  
  testing:{
    client: 'postgresql',
    connection: {
      host: process.env.POSTGRES_HOST ,
      database: process.env.TSDB_Name ,
      user:     process.env.TSDB_User ,
      password: process.env.TSDB_Password
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    } 
  }
};
