import * as React from 'react'
import { InputGroup, InputGroupAddon, Button, Input } from 'reactstrap';
import './Css/CustomSearch.css'



interface ICustomSearchState {
    dropdownOpen: boolean
}
interface ICustomSearchProps {

}

export default class CustomSearch extends React.Component<ICustomSearchProps, ICustomSearchState> {
    state = {
        dropdownOpen: false
    }





    render() {
        return (
            
            <div className="container">
               
            <div className="search">
            <div className="space" /> 
                <InputGroup >
                    <Input />
                    <InputGroupAddon addonType="append">
                        <Button color="secondary">Search</Button>
                    </InputGroupAddon>
                </InputGroup>

           
            </div>
            </div>

        )
    }

}