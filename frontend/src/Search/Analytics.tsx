import * as React from 'react'
import { Table } from 'reactstrap';
import './Css/Analytics.css'

interface ISearchState {

}
interface ISearchProps {

}

export default class Search extends React.Component<{}, {}> {
    state: ISearchState = {

    }



    render() {
        return (
            <div>
                 <div className="space" />
                <div className="container">
                   
                    <Table striped id="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>1</th>
                                <th>2</th>
                                <th>3</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Most Views</th>
                                <td></td>
                                <td>Coming Soon</td>
                                <td></td>

                            </tr>
                            <tr>
                                <th scope="row">Most Views below $100k</th>
                                <td></td>
                                <td>Coming Soon</td>
                                <td></td>

                            </tr>
                            <tr>
                                <th scope="row">Popular Japanese Cars</th>
                                <td></td>
                                <td>Coming Soon</td>
                                <td></td>

                            </tr>
                            <tr>
                                <th scope="row">Popular SUVs</th>
                                <td></td>
                                <td>Coming Soon</td>
                                <td></td>

                            </tr>
                            <tr>
                                <th scope="row">Popular Saloon</th>
                                <td></td>
                                <td>Coming Soon</td>
                                <td></td>

                            </tr>
                            <tr>
                                <th scope="row">Popular Sport Cars</th>
                                <td></td>
                                <td>Coming Soon</td>
                                <td></td>

                            </tr>
                            <tr>
                                <th scope="row">Popular Convertibles</th>
                                <td></td>
                                <td>Coming Soon</td>
                                <td></td>

                            </tr>
                        </tbody>
                    </Table>
                    

                </div>
                <div className="space" />
            </div>
        )
    }
}