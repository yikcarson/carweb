import * as React from 'react'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Button, InputGroup, InputGroupAddon, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { fetchResult } from '../redux/Search/action'
import { IRootState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import './Css/CriteriaSearch.css'
import { string } from 'prop-types';

interface ISearchState {
    dropdownOpenBrand: boolean
    dropdownOpenSeats: boolean
    dropdownOpenPrice: boolean
    dropdownOpenEngineVolume: boolean
    dropdownOpenManuYear: boolean
    dropdownOpenTransmission: boolean
    price: string
    auto: string
    engineVol: string
    criteria: {
        brand: string | undefined
        seats: number | null | undefined
        startPrice: number | null | undefined
        endPrice: number | null | undefined
        endEvol: number | null | undefined,
        startEvol: number | null | undefined,
        ManuYear: number | null | undefined,
        isAuto: boolean | null | undefined
    }
}
interface ISearchProps {
    fetchResult: (criteria: object) => void
}

class CriteriaSearch extends React.Component<ISearchProps, ISearchState> {
    state = {
        dropdownOpenBrand: false,
        dropdownOpenSeats: false,
        dropdownOpenPrice: false,
        dropdownOpenEngineVolume: false,
        dropdownOpenManuYear: false,
        dropdownOpenTransmission: false,
        price: "",
        auto: "",
        engineVol: "",

        criteria: {
            brand: "",
            seats: undefined,
            endPrice: undefined,
            startPrice: undefined,
            endEvol: undefined,
            startEvol: undefined,
            ManuYear: undefined,
            isAuto: undefined
        }
    }

    private dropdown = {
        1 : "dropdownOpenBrand",
        2: "dropdownOpenSeats",
        3: "dropdownOpenPrice",
        4: "dropdownOpenEngineVolume",
        5: "dropdownOpenManuYear",
        6: "dropdownOpenTransmission"
    }

    toggle = (id: number) => {


        // this.setState(prevState => ({
        //     dropdown[id]: !prevState[ dropdown[id]]
        // }));
        switch (id) {
            case 1:
                this.setState(prevState => ({
                    dropdownOpenBrand: !prevState.dropdownOpenBrand
                }));
                break;
            case 2:
                this.setState(prevState => ({
                    dropdownOpenSeats: !prevState.dropdownOpenSeats
                }));
                break;
            case 3:
                this.setState(prevState => ({
                    dropdownOpenPrice: !prevState.dropdownOpenPrice
                }));
                break;

            case 4:
                this.setState(prevState => ({
                    dropdownOpenEngineVolume: !prevState.dropdownOpenEngineVolume
                }));
                break;
            case 5:
                this.setState(prevState => ({
                    dropdownOpenManuYear: !prevState.dropdownOpenManuYear
                }));
                break;
            case 6:
                this.setState(prevState => ({
                    dropdownOpenTransmission: !prevState.dropdownOpenTransmission
                }));
                break;
        }
    }

    setBrand = (e: React.MouseEvent<any, MouseEvent>) => {
        let text = e.currentTarget.innerText === "Others" ? "" : e.currentTarget.innerText

        this.setState({
            ...this.state,
            criteria: {
                ...this.state.criteria,
                brand: text
            }
        });
    }
    setSeats = (e: React.MouseEvent<any, MouseEvent>) => {
        if (e.currentTarget.innerText.replace(/[^0-9]/g, '') !== null) {
            this.setState({
                ...this.state,
                criteria: {
                    ...this.state.criteria,
                    seats: parseInt(e.currentTarget.innerText)
                }
            });
        }
    }
    setPrice = (e: React.MouseEvent<any, MouseEvent>) => {
        let text = e.currentTarget.innerText
        let arr = text.split(' ')

        let first = parseInt(arr[0].replace(/[^0-9]/g, ''))
        let second = parseInt(arr[2].replace(/[^0-9]/g, ''))

        let minPrice = first
        let maxPrice = second || 100000000

        this.setState({
            ...this.state,
            price: text,
            criteria: {
                ...this.state.criteria,
                endPrice: maxPrice,
                startPrice: minPrice
            }
        });
    }

    setEngineVol = (e: React.MouseEvent<any, MouseEvent>) => {

        let text = e.currentTarget.innerText
        let arr = text.split(' ')

        let first = parseInt(arr[0].replace(/[^0-9]/g, ''))
        let second = parseInt(arr[1].replace(/[^0-9]/g, ''))

        let startEvol = first
        let endEvol = second || 1000000
        this.setState({
            ...this.state,
            engineVol: text,
            criteria: {
                ...this.state.criteria,
                startEvol: startEvol,
                endEvol: endEvol
            }
        });
    }
    setManuYear = (e: React.MouseEvent<any, MouseEvent>) => {
        this.setState({
            ...this.state,
            criteria: {
                ...this.state.criteria,
                ManuYear: parseInt(e.currentTarget.innerText)
            }
        });
    }

    setCustManuYear = (e: React.ChangeEvent<HTMLInputElement>) => {

        this.setState({
            ...this.state,
            criteria: {
                ...this.state.criteria,
                ManuYear: parseInt(e.currentTarget.value) || undefined
            }
        });
    }
    setTrans = (e: React.MouseEvent<any, MouseEvent>) => {
        let text = e.currentTarget.innerText;
        let bool = (text == "Auto") ? true : false

        this.setState({
            ...this.state,
            auto: text,
            criteria: {
                ...this.state.criteria,
                isAuto: bool
            }
        });
    }

    render() {
        const { dropdownOpenTransmission, criteria } =this.state
        return (
            <div className="buttonContainer">
                <div >
                    <div className="space" />
                    <Dropdown isOpen={this.state.dropdownOpenBrand} toggle={() => this.toggle(1)} >
                        <DropdownToggle caret className="button">
                            Brand: {this.state.criteria.brand}&nbsp;
                    </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem onClick={this.setBrand}>Audi</DropdownItem>
                            <DropdownItem onClick={this.setBrand}>BMW</DropdownItem>
                            <DropdownItem onClick={this.setBrand}>Honda</DropdownItem>
                            <DropdownItem onClick={this.setBrand}>MercedesBenz</DropdownItem>
                            <DropdownItem onClick={this.setBrand}>Porsche</DropdownItem>
                            <DropdownItem onClick={this.setBrand}>Toyota</DropdownItem>

                        </DropdownMenu>
                    </Dropdown>
                    <div className="space" />
                    <Dropdown isOpen={this.state.dropdownOpenSeats} toggle={() => this.toggle(2)}>
                        <DropdownToggle caret className="button">
                            Seats: {this.state.criteria.seats}&nbsp;
                    </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem onClick={this.setSeats}>2</DropdownItem>
                            <DropdownItem onClick={this.setSeats}>4</DropdownItem>
                            <DropdownItem onClick={this.setSeats}>5</DropdownItem>
                            <DropdownItem onClick={this.setSeats}>6</DropdownItem>
                            <DropdownItem onClick={this.setSeats}>7</DropdownItem>
                            <DropdownItem onClick={this.setSeats}>8</DropdownItem>
                        </DropdownMenu>
                    </Dropdown>
                    <div className="space" />
                    <Dropdown isOpen={this.state.dropdownOpenPrice} toggle={() => this.toggle(3)}>
                        <DropdownToggle caret className="button">
                            Price: {this.state.price} &nbsp;
        </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem onClick={this.setPrice}>  $0 - $100,000  </DropdownItem>
                            <DropdownItem onClick={this.setPrice}>$100,000 - $200,000</DropdownItem>
                            <DropdownItem onClick={this.setPrice}>$200,000 - $400,000</DropdownItem>
                            <DropdownItem onClick={this.setPrice}>$400,000 - $700,000</DropdownItem>
                            <DropdownItem onClick={this.setPrice}>$700,000 - $1,000,000</DropdownItem>
                            <DropdownItem onClick={this.setPrice}> $1,000,000 or above</DropdownItem>
                        </DropdownMenu>
                    </Dropdown>
                    <div className="space" />

                    <div className="space" />
                    <Dropdown isOpen={this.state.dropdownOpenEngineVolume} toggle={() => this.toggle(4)}>
                        <DropdownToggle caret className="button">
                            Engine Volume: {this.state.engineVol}&nbsp;
                    </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem onClick={this.setEngineVol}>600cc or below</DropdownItem>
                            <DropdownItem onClick={this.setEngineVol}>601 - 1500cc</DropdownItem>
                            <DropdownItem onClick={this.setEngineVol}>1501 - 2000cc</DropdownItem>
                            <DropdownItem onClick={this.setEngineVol}>2001 - 2500cc</DropdownItem>
                            <DropdownItem onClick={this.setEngineVol}>2501 - 3500cc</DropdownItem>
                            <DropdownItem onClick={this.setEngineVol}>3501 - 4500cc</DropdownItem>
                            <DropdownItem onClick={this.setEngineVol}>4500cc or above</DropdownItem>
                        </DropdownMenu>
                    </Dropdown>
                    <div className="space" />
                    <Dropdown isOpen={this.state.dropdownOpenManuYear} toggle={() => this.toggle(5)}>
                        <DropdownToggle caret className="button">
                            Manufacture Year: {this.state.criteria.ManuYear}&nbsp;
                    </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem onClick={this.setManuYear}>2018</DropdownItem>
                            <DropdownItem onClick={this.setManuYear}>2017</DropdownItem>
                            <DropdownItem onClick={this.setManuYear}>2016</DropdownItem>
                            <DropdownItem onClick={this.setManuYear}>2015</DropdownItem>
                            <DropdownItem onClick={this.setManuYear}>2014</DropdownItem>
                            <DropdownItem onClick={this.setManuYear}>2013</DropdownItem>
                            <DropdownItem onClick={this.setManuYear}>2012</DropdownItem>
                            <DropdownItem onClick={this.setManuYear}>2011</DropdownItem>
                            <DropdownItem onClick={this.setManuYear}>2010</DropdownItem>
                            <DropdownItem onClick={this.setManuYear}>2009</DropdownItem>
                            <InputGroup>
                                <InputGroupAddon addonType="prepend" >Custom</InputGroupAddon>
                                <Input onChange={this.setCustManuYear} maxLength={4}/>
                            </InputGroup>
                        </DropdownMenu>
                    </Dropdown>
                    <div className="space" />
                    <Dropdown isOpen={this.state.dropdownOpenTransmission} toggle={() => this.toggle(6)}>
                        <DropdownToggle caret className="button">
                            Transmission: {this.state.auto}&nbsp;
                    </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem onClick={this.setTrans}>Auto</DropdownItem>
                            <DropdownItem onClick={this.setTrans}>Manual</DropdownItem>
                        </DropdownMenu>
                    </Dropdown>
                    <div className="space" />
                    <Button color="warning" onClick={() => this.props.fetchResult(this.state.criteria)}> Go! </Button>
                </div>
            </div>

        )
    }

}

const mapDispatchToProps = {
    fetchResult
}

export default connect(null, mapDispatchToProps)(CriteriaSearch)
