import * as React from 'react';
import { connect } from 'react-redux';
import PostFrom from './PostFrom';
import { Button, FormGroup, Input } from 'reactstrap';
import PostPriceTrend from './PostPriceTrend';

interface IPostState {
    open: boolean
    active: boolean
}
class Post extends React.Component<{}, IPostState> {
    state: IPostState = {
        open: false,
        active: false
    }

    open = () => {
        // document.getElementById('sidebar')!.classList.toggle('active')
        this.setState({active: !this.state.active})
    }
    render() {
        return (
            <div>
                <div id="sidebar" className={this.state.active ? "active" : ""}>
                    <div className="toggle-btn">
                        <Button id="siderButton" onClick={this.open} />
                    </div>
                    <div>
                        <PostFrom />
                    </div>
                </div>
                <div id="priceTrend">
                    <PostPriceTrend />
                </div>

               
            </div>
        )
    }
}

// const mapStateToprops = (state) => ({})

export default connect()(Post)







