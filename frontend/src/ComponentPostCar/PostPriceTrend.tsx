import * as React from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch, IRootState } from '../store';
import { Button } from 'reactstrap';
import { Line, ChartData } from 'react-chartjs-2';
import * as chartjs from 'chart.js'

interface IPostPriceTrendProps {
    brand: string,
    carname: string
}
interface IPostPriceTrendState {
    price: number[],
    label: string[]
}

class PostPriceTrend extends React.Component<IPostPriceTrendProps, IPostPriceTrendState> {
    state: IPostPriceTrendState = {
        price: [],
        label: []
    }
    checkprice = async () => {
        if (this.props.brand != '' && this.props.carname != '') {
            const res = await fetch(`${process.env.REACT_APP_SERVER}/postcar/pricecheck`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({ brand: this.props.brand, carname: this.props.carname })
            })
            const result = await res.json()
            this.setState({
                price:result.price,
                label:result.year
            })
        }
    }
    render() {
        const data: ChartData<chartjs.ChartData> = {
            labels: this.state.label,
            datasets: [
                {
                    label: '',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderColor: 'rgba(75,192,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: this.state.price
                }
            ]
        };
        return (
            <div id='pricePostmid'>
                <div id="priceTitle">
                    <span> 價格趨勢 (依出產年份排序)</span>
                </div>
                <div id='price_additon'>
                    <div> <span>牌子：{this.props.brand}</span></div>
                    <div> <span>型號：{this.props.carname}</span></div>
                </div>

                <div><Button onClick={this.checkprice}>查看價錢</Button></div>
                <div id="ccc123">
                    <Line data={data}
                        options={{ maintainAspectRatio: false }} />
                </div>

            </div>
        )
    }
}
const mapStateToProps = (state: IRootState) => ({
    brand: state.post.brand,
    carname: state.post.carname
})

const mapDispatchToprops = (dispatch: ThunkDispatch) => ({

})

export default connect(mapStateToProps, mapDispatchToprops)(PostPriceTrend)