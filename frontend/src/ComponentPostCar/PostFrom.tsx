import * as React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, InputGroup, InputGroupButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Row } from 'reactstrap';
import { connect } from 'react-redux';
import { ThunkDispatch, IRootState } from '../store';
import { ValueType, ActionMeta } from 'react-select/src/types';
import Select from 'react-select';
import { postUpdateBrand, postUpdateCarname } from '../redux/post/action';
import axios from 'axios'

type fieldname = "brand" | "Title" | "price" | "seats" | "Transmission" | "contactName" | "contactNo" | "detail" | "year" | "volume" | "km" | 'rental' | 'rentalPrice'


const yearOptions = [
    { value: "", label: "None" },
    { value: "2000", label: "2000" },
    { value: "2001", label: "2001" },
    { value: "2002", label: "2002" },
    { value: "2003", label: "2003" },
    { value: "2004", label: "2004" },
    { value: "2005", label: "2005" },
    { value: "2006", label: "2006" },
    { value: "2007", label: "2007" },
    { value: "2008", label: "2008" },
    { value: "2009", label: "2009" },
    { value: "2010", label: "2010" },
    { value: "2011", label: "2011" },
    { value: "2012", label: "2012" },
    { value: "2013", label: "2013" },
    { value: "2014", label: "2014" },
    { value: "2015", label: "2015" },
    { value: "2016", label: "2016" },
    { value: "2017", label: "2017" },
    { value: "2018", label: "2018" },
    { value: "2019", label: "2019" }
];

interface IPostFromState {
    brand: string,
    carname: string,
    selectedOption: ValueType<{ value: string, label: string }>,
    year: number | undefined,
    isAuto: boolean | undefined,
    seats: number | undefined,
    volume: number | undefined,
    detail: string | undefined
    price: number | undefined,
    contactName: string | undefined,
    contactNo: number | undefined,
    km: number | undefined,
    file: File | null,
    rental: boolean,
    rentalPrice: undefined | number
    dropdownOpen: boolean
    rental_day: undefined|number
}

interface Fields {
    brand: string,
    carname: string,
    year: number | undefined,
    isAuto: boolean | undefined,
    seats: number | undefined,
    volume: number | undefined,
    detail: string | undefined
    price: number | undefined,
    contactName: string | undefined,
    contactNo: number | undefined,
    km: number | undefined,
    file: File | null,
    rental: boolean,
    rentalPrice: undefined | number
    rental_day: undefined|number
}
interface IPostFromProps {
    username: string | null
    reduxBrand: (brand: string) => void
    reduxCarname: (carname: string) => void
}


class PostFrom extends React.Component<IPostFromProps, IPostFromState> {
    state: IPostFromState = {
        brand: 'Audi',
        carname: '',
        selectedOption: { value: "", label: "None" },
        year: undefined,
        isAuto: undefined,
        seats: undefined,
        volume: undefined,
        detail: undefined,
        price: undefined,
        contactName: undefined,
        contactNo: undefined,
        km: undefined,
        file: null,
        rental: false,
        rentalPrice: undefined,
        dropdownOpen: false,
        rental_day: undefined
    }
    onChange = (filed: fieldname, event: React.ChangeEvent<HTMLInputElement>) => {
        if (filed == "brand") {
            this.setState({
                brand: event.currentTarget.value
            })
            this.props.reduxBrand(event.currentTarget.value)
        }
        if (filed == "Title") {
            this.setState({
                carname: event.currentTarget.value
            })
            this.props.reduxCarname(event.currentTarget.value)
        }
        if (filed == "price") {
            this.setState({
                price: parseInt(event.currentTarget.value) || 0
            })
        }
        if (filed == "seats") {
            if (event.currentTarget.value == "None") {
                this.setState({
                    seats: undefined
                })
                return
            }
            this.setState({
                seats: parseInt(event.currentTarget.value) | 0
            })
        }
        if (filed == "Transmission") {
            if (event.currentTarget.value == "None") {
                this.setState({
                    isAuto: undefined
                })
                return
            }
            this.setState({
                isAuto: event.currentTarget.value == "AT" ? true : false
            })
        }
        if (filed == "contactName") {
            if (event.currentTarget.value == "") {
                this.setState({
                    contactName: undefined
                })
                return
            }
            this.setState({
                contactName: event.currentTarget.value
            })
        }
        if (filed == "contactNo") {

            this.setState({
                contactNo: parseInt(event.currentTarget.value) || 0
            })
        }
        if (filed == "detail") {
            if (event.currentTarget.value == "") {
                this.setState({
                    detail: undefined
                })
                return
            }
            this.setState({
                detail: event.currentTarget.value
            })
        }
        if (filed == "volume") {
            if (event.currentTarget.value == "") {
                this.setState({
                    volume: undefined
                })
                return
            }
            this.setState({
                volume: parseInt(event.currentTarget.value) || 0
            })
        }
        if (filed == "km") {
            if (event.currentTarget.value == "") {
                this.setState({
                    km: undefined
                })
                return
            }
            this.setState({
                km: parseInt(event.currentTarget.value) || 0
            })
        }
        if (filed == 'rental') {
            if(event.currentTarget.value =='No'){
                this.setState({
                    rental:  false,
                    rental_day: undefined
                })
            }
            this.setState({
                rental: event.currentTarget.value == 'Yes' ? true : false,
                rental_day: 1
            })
        }
        if (filed == 'rentalPrice') {
            this.setState({
                rentalPrice: parseInt(event.currentTarget.value) || 0
            })
        }
    }

    // onFieldChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    //     this.setState({
    //         [e.target.name]: e.currentTarget.value
    //     } as Pick<Fields, keyof Fields>)
    // }

    toggleDropDown = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    handleChange = (selectedOption: ValueType<{ value: string, label: string }>, actionMeta: ActionMeta) => {
        // if ((selectedOption as { value: string, label: string }).value == "") {
        //     this.setState({
        //         selectedOption: selectedOption,
        //         year: undefined
        //     })
        //     return
        // }
        // this.setState({
        //     selectedOption: selectedOption,
        //     year: parseInt((selectedOption as { value: string, label: string }).value)
        // });
            const a = (selectedOption as { value: string, label: string }).value
            this.setState({
                selectedOption: selectedOption,
                year:  (a == "") ? parseInt(a) : undefined
            })
     
        console.log(`Option selected:`, selectedOption);
    };
    try = () => {
        const data = { ...this.state }
        delete data["selectedOption"]
        console.log(data)
    }
    postCar = async () => {
        if (this.state.carname == '' || this.state.year == undefined || !this.state.seats == undefined || this.state.volume == undefined) {
            return alert('Input with *')
        }
        const PostFrom: any = document.getElementById('PostFrom')
        let formData = new FormData(PostFrom)
        if (this.state.file != null) {
            formData.append('file', this.state.file)
        }
        formData.append('username', this.props.username!)
        if(this.state.rental_day){
            formData.append('rental_day', (this.state.rental_day).toString())
        }
        const res = await axios.post(`${process.env.REACT_APP_SERVER}/postcar/car`, formData, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        if (res.data.msg == 'success') {
            this.setState({
                brand: 'Audi',
                carname: '',
                selectedOption: { value: "", label: "None" },
                year: undefined,
                isAuto: undefined,
                seats: undefined,
                volume: undefined,
                detail: undefined,
                price: undefined,
                contactName: undefined,
                contactNo: undefined,
                km: undefined,
                file: null,
                rental: false,
                rentalPrice: undefined,
                dropdownOpen: false,
                rental_day:undefined,
            })
            this.props.reduxBrand('Audi')
            this.props.reduxCarname('')
            return alert('success')
        } else {
            return res.status
        }
    }

    private handleFileOnChange = (files: FileList) => {
        this.setState({ file: files[0] })
    }
    changeDay = (number: number, e: React.MouseEvent) => {
        this.setState({
            rental_day: number
        })
    }

    render() {
        return (
            <Form id="PostFrom">
                <FormGroup>
                    <Label for="brand">Brand</Label>
                    <Input type="select" name='brand' onChange={this.onChange.bind(this, "brand")} value={this.state.brand}>
                        <option>Audi</option>
                        <option>MercedesBenz</option>
                        <option>BMW</option>
                        <option>Honda</option>
                        <option>Porsche</option>
                        <option>Toyota</option>
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label for="Title">Car Model *</Label>
                    <Input type="text" name='carname' onChange={this.onChange.bind(this, "Title")} value={this.state.carname} placeholder="enter your car post title" />
                </FormGroup>
                <FormGroup>
                    <Label for="manufacture_year">Manufacture Year *</Label>
                    <Select name='year'
                        value={this.state.selectedOption}
                        onChange={this.handleChange}
                        options={yearOptions}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="price">Price *</Label>
                    <Input type="text" name='price' onChange={this.onChange.bind(this, "price")} value={this.state.price} placeholder="car price" />
                </FormGroup>
                <FormGroup>
                    <Label for="seats">Seats *</Label>
                    <Input type="select" name='seats' onChange={this.onChange.bind(this, "seats")} value={this.state.seats} >
                        <option>None</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label for="volume">Volume *</Label>
                    <Input type="text" name='volume' onChange={this.onChange.bind(this, "volume")} value={this.state.volume} placeholder="engine volume (in cc)" />
                </FormGroup>
                <FormGroup>
                    <Label for="Transmission">Transmission</Label>
                    <Input type="select" name='isAuto' onChange={this.onChange.bind(this, "Transmission")} >
                        <option>None</option>
                        <option>AT</option>
                        <option>MT</option>
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label for="Rental">Rental</Label>
                    <Input type="select" name='rental' onChange={this.onChange.bind(this, 'rental') }>
                    <Input />
                        <option>No</option>
                        <option>Yes</option>
                    </Input>
                </FormGroup>

                <InputGroup hidden={this.state.rental == false ? true : false}>
                    <Input name='rentalPrice' value={this.state.rentalPrice} onChange={this.onChange.bind(this,'rentalPrice')}placeholder="rental price for"  />
                    <InputGroupButtonDropdown addonType="append" isOpen={this.state.dropdownOpen} toggle={this.toggleDropDown}>
                        <DropdownToggle caret>
                            {this.state.rental_day} Days
            </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem onClick={this.changeDay.bind(this, 1)}>1 Days</DropdownItem>
                            <DropdownItem onClick={this.changeDay.bind(this, 7)}>7 Days</DropdownItem>
                            <DropdownItem onClick={this.changeDay.bind(this, 14)}>14 Days</DropdownItem>
                        </DropdownMenu>
                    </InputGroupButtonDropdown>
                </InputGroup>




                <FormGroup>
                    Please upload a picture of your car for posting
                    <Input type='file' onChange={(e) => this.handleFileOnChange(e.target.files as FileList)} />
                </FormGroup>





                <FormGroup>
                    <Label for="contactName">Contact Name</Label>
                    <Input type="text" name='contactName' onChange={this.onChange.bind(this, "contactName")} value={this.state.contactName} placeholder="enter your contact name" />
                </FormGroup>
                <FormGroup>
                    <Label for="contactNo">Contact No</Label>
                    <Input type="text" name='contactNo' maxLength={8} onChange={this.onChange.bind(this, "contactNo")} value={this.state.contactNo} placeholder="enter your contact no" />
                </FormGroup>
                <FormGroup>
                    <Label for="detail">已行駛里程Km</Label>
                    <Input type="text" name='km' maxLength={5} onChange={this.onChange.bind(this, "km")} value={this.state.km} />
                </FormGroup>
                <FormGroup>
                    <Label for="detail">Additonal Info</Label>
                    <Input type="textarea" name='detail' onChange={this.onChange.bind(this, "detail")} value={this.state.detail} />
                </FormGroup>

                <Button onClick={this.postCar}>Post your Car</Button>
            </Form>
        )
    }
}

const mapStateToPrtops = (state: IRootState) => ({
    username: state.auth.username

})
const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    reduxBrand: (brand: string) => { dispatch(postUpdateBrand(brand)) },
    reduxCarname: (carname: string) => { dispatch(postUpdateCarname(carname)) }
})

export default connect(mapStateToPrtops, mapDispatchToProps)(PostFrom)