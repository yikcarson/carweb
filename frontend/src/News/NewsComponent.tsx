import React from 'react'
import { Media, Row, Col } from 'reactstrap';
import {articles} from './News'
import './Css/NewsComponent.css'
import Moment from 'moment'

interface INewsProps {
    Info: articles
    id:number
}
interface INewsState {

}

export default class NewsComponent extends React.Component<INewsProps, INewsState> {

    render() {
        const {Info} = this.props
        let details={
            name: Info.source.name,
            author: Info.author,
            title: Info.title,
            description: Info.description,
            url: Info.url,
            urlToImage: Info.urlToImage,
            publishedAt: Info.publishedAt,
            content:Info.content
        }

        return (
            <div className="component-container">
                <Media id="news-media">
                    <Col xs="2">
                    <Media left href="#">
                    <img
                    src={Info.urlToImage} width={160} height={150} />
                    </Media>
                    </Col>
                    <Col xs="10">
                    <Media body>
                    <Row>
                        <Media heading>
                           <a href={Info.url} target="_blank">{Info.title}</a> 
                        </Media>
                        </Row>
                        <Row>{Info.description}</Row>
                        <br />
                        <Row>Source: {details.name}</Row>
                        <Row>Date: {Moment(Info.publishedAt).format('DD-MM-YYYY')}</Row>
                    </Media>
                    </Col>
                </Media>
            </div>
        )
    }
}