import React from 'react'
import NewsComponent from './NewsComponent'
import { Spinner } from 'reactstrap';
import './Css/News.css'

interface json {
    status: string,
    totalResults: number,
    articles: articles[]
}

export interface articles {
    source: {
        id: string,
        name: string,
    },
    author: string,
    title: string,
    description: string,
    url: string,
    urlToImage: string,
    publishedAt: Date,
    content: string
}

interface INewsProps {

}
interface INewsState {
    news: object[] |null
    newsPosts:any[] |null
}

export default class News extends React.Component<INewsProps, INewsState> {
    state = {
        news: [],
        newsPosts:null
    }

    componentDidMount =async() =>{
        // await this.fetchCarNews();
        await this.generateNewsBlocks();
    }

    fetchCarNews = async (): Promise<articles[]> => {
        const res = await fetch(`https://newsapi.org/v2/everything?q=motor+auto+vehicle&from=2019-06-30&sortBy=popularity&language=en&apiKey=fd479a4916ce4577b528cc9cf65bc1d4`);
        const json = await res.json();

        // this.setState({
        //     news: json.articles
        // })
        console.log(json.articles);
        return json.articles;
    }

    generateNewsBlocks = async () => {
        // let newsArr = []
        let news = await this.fetchCarNews();
        this.setState({
            newsPosts: Object.keys(news).map((e:any,i) => {
                return <NewsComponent Info={news[i]} id={i}/>
        })

    })
}

loading = ()=>{
    if(this.state.newsPosts == null) {
            return <Spinner color="info" />
    }
}

    render() {
        
        console.log(this.state.newsPosts);
        return (
            <div>
                {this.loading()}
            {this.state.newsPosts}
            
            </div>

        )
    }
}

// this.state.news.map((e) => {
//     return <NewsComponent Info={e} />
// })