import * as React from 'react'
import CustomSearch from './CustomSearch'
import CriteriaSearch from './CriteriaSearch';
import Analytics from './Analytics'
import Result from '../Result/Result';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import classnames from 'classnames';


interface ISearchState {
    activeTab:string
}


export default class SearchPage extends React.Component<{}, ISearchState> {
    state: ISearchState = {
        activeTab: '1'
    }

    toggle(tab:string) {
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab
          });
        }
      }
    

    render() {
        return (
            <div>
                <div className="space" />
                <h2> Search for your dream cars!</h2>
            <div>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.toggle('1'); }}
            >
              Search
            </NavLink>
          </NavItem>
          {/* <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.toggle('2'); }}
            >
              Analytics
            </NavLink>
          </NavItem> */}
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col sm="12">
               
               <CriteriaSearch />
              </Col>
            </Row>
          </TabPane>
          {/* <TabPane tabId="2">
            <Row>
              <Col sm="12">
              <Analytics />
              </Col>
            </Row>
          </TabPane> */}
        </TabContent>
      </div>
   
            </div>

        )

    }
}