export interface profileData {

        id: number,
        name: string,
        year: number,
        isAuto: boolean,
        seats: number,
        volume: number,
        detail: number,
        price: number,
        url: string | null,
        contactName: string | null,
        contactNo: string | null,
        isSold: boolean
        km: number
        brandName: string,
        websiteName: string,
        imgLink: string
        updateDate: string
}