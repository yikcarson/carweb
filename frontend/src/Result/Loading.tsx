import * as React from 'react';
import { Spinner } from 'reactstrap';
import './Css/Loading.css'

export default class Loading extends React.Component<{}, {}> {

    render() {
        return (
            <div>
               <h1>We are now searching for your dream car, please bear with us for a minute...</h1>
                {/* <video src={require('../video/loading.mp4')} width="1200"autoPlay={true} loop={true} ></video> */}

                    <Spinner color="info" />   
                      
            </div>
                    )
                }
}