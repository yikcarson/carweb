import React from 'react'
import { Media, Card, CardTitle, CardText, Collapse, CardBody, Nav, NavItem, NavLink, TabContent, TabPane, UncontrolledCarousel, Table, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import { Container, Row, Col, Button } from 'reactstrap';
import car1 from './720s_spider.jpg'
import car2 from './BENZ_AMG.jpg'
//import Holder from 'react-holder'
import './Css/PanelComponent.css'
import classnames from 'classnames'
import { Line, ChartData } from 'react-chartjs-2';
import * as chartjs from 'chart.js'
import DetailComponent from './DetailComponent';
import { CarSearch, carObject, criteria, CarDetail } from '../redux/Search/state';
import { thisExpression } from '@babel/types';



interface IPanelComProps {
    detail: CarDetail
    carName: string
    carBrand: string | undefined
}

interface IPanelComState {
    activeTab: string
    collapse: boolean
    componentPage: number | null,
    pageNo: number[]
    // yearLabel: number[]
    // yearPrice: number[]
    // kmLabel: number[]
    // kmPrice: number[]
}


export default class PanelComponent extends React.Component<IPanelComProps, IPanelComState> {

    state = {
        activeTab: "1",
        collapse: false,
        componentPage: null || 1,
        pageNo: [1, 2, 3, 4, 5],
        // yearLabel: [],
        // yearPrice: [],
        // kmLabel: [],
        // kmPrice: []
    }

    toggle(tab: string) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    toggleCollapse() {
        this.setState(prevState => ({
            collapse: !prevState.collapse
        }));
    }



    detailComponentGenerator = (page: number) => {

        const partialCars = this.props.detail.datas.slice((page - 1) * 5, 5 * page)
        return partialCars.map((e, i) => {
            return <DetailComponent carDetail={e} />
        })
        // let arr = this.props.detail.datas
        // 1-5 6-10 11-15 16-20
        // let startItemIndex = page === 1 ? 0 : page + (4 * (page - 1) )
        // let endItemIndex = page === 1 ? 5 : page + (4 * (page - 1) + 4 )
        // let slice = arr.slice(startItemIndex,endItemIndex)
        // return slice && slice.map((result: CarSearch, i) => {
        //     return <DetailComponent key={i} carDetail={result} />
        // })
    }

    changeComponentPage = (e: React.MouseEvent<any, MouseEvent> | number) => {
        let type = typeof (e) == "number" ? e : e.currentTarget.innerText
        if(type <= Math.ceil(this.props.detail.datas.length / 5) && type > 0){
        this.setState({
            componentPage: type
        })
        }
        if (this.state.componentPage === type ) {
            return;
        } else if (this.state.componentPage < type && type <= Math.ceil(this.props.detail.datas.length / 5) && type > 0 ) {
            this.setState({
                pageNo: this.state.pageNo.map((e) => {
                    return e + (type - this.state.componentPage)
                })
            })
        } else if (this.state.componentPage > type && type <= Math.ceil(this.props.detail.datas.length / 5) && type > 0) {
            this.setState({
                pageNo: this.state.pageNo.map((e) => {
                    return e - (this.state.componentPage - type)
                })
            })
        }


    }


    render() {
        const label:string[] = this.props.detail.label.map((e)=>{
            return JSON.stringify(e);
        })

        const data1: ChartData<chartjs.ChartData> = {
            labels: label,
            datasets: [
                {
                    label: 'Price Distribution by Manufacture Year',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderColor: 'rgba(75,192,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: this.props.detail.prices
                    
                }
            ]
        };

       


        return (
            <div >
                <div className="container">
                    <Media>
                        <Media body className="component-body">
                            <Row className="big-row">
                                <Col xs="3" className="pic">
                                    <img
                                        src={this.props.detail.datas[0]._source.imgLink} width={190} height={190} alt="car1" />
                                </Col>
                                <Col xs="9" className="content-col">
                                    <Row className="car-name"> {this.props.carBrand}&nbsp;{this.props.carName}</Row>
                                    <Row className="space"></Row>
                                    <Row className="desc">Lowest prices:</Row>
                                    <Row className="space"></Row>
                                    <Row className="card-container">
                                        <Card body className="small-card">
                                            <CardTitle>Starting from<br />
                                            <span>${this.props.detail.prices[0]}</span></CardTitle>
                                        </Card>
                                        <Card body className="small-card">
                                            <CardTitle>car1.hk<br />
                                            (coming soon)</CardTitle>
                                        </Card>
                                        <Card body className="small-card">
                                            <CardTitle>price.com.hk<br />
                                            (coming soon)</CardTitle>
                                        </Card>
                                        <Card body className="small-card">
                                            <CardTitle>大昌行<br />
                                            (coming soon)</CardTitle>
                                        </Card>
                                    </Row>
                                    <Row>
                                        <Button color="primary" onClick={() => this.toggleCollapse()} style={{ marginBottom: '1rem' }}>
                                            More details
    </Button>
                                    </Row>
                                </Col>

                            </Row>

                        </Media>

                    </Media>

                </div>
                <Collapse isOpen={this.state.collapse}>
                    <Card>
                        <CardBody>
                            <div>
                                <Nav tabs>
                                    <NavItem>
                                        <NavLink
                                            className={classnames({ active: this.state.activeTab === '1' })}
                                            onClick={() => { this.toggle('1'); }}
                                        >
                                            Statistics
            </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className={classnames({ active: this.state.activeTab === '2' })}
                                            onClick={() => { this.toggle('2'); }}
                                        >
                                            Listings
            </NavLink>
                                    </NavItem>
                                </Nav>
                                <TabContent activeTab={this.state.activeTab}>
                                    <TabPane tabId="1">

                                        <Col xs="12" className="graph-col">
                                            <Line data={data1} width={10}
                                                height={20}
                                                options={{ maintainAspectRatio: false }} />
                                        </Col>


                                    </TabPane>
                                    <TabPane tabId="2">
                                        <Row>
                                            <Col sm="12">
                                                <p>Page {this.state.componentPage} of {Math.ceil(this.props.detail.datas.length / 5)} ({this.props.detail.datas.length}&nbsp;Results)</p>
                                                {this.detailComponentGenerator(this.state.componentPage)}
                                                <Pagination aria-label="Page navigation example">
                                                    <PaginationItem>
                                                        <PaginationLink first onClick={() => this.changeComponentPage(1)} />
                                                    </PaginationItem>
                                                    <PaginationItem>
                                                        <PaginationLink previous onClick={() => this.changeComponentPage(this.state.componentPage - 1)} />
                                                    </PaginationItem>
                                                    <PaginationItem>
                                                        <PaginationLink onClick={this.changeComponentPage}>
                                                            {this.state.pageNo[0]}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                    <PaginationItem>
                                                        <PaginationLink onClick={this.changeComponentPage}>
                                                            {this.state.pageNo[1]}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                    <PaginationItem>
                                                        <PaginationLink onClick={this.changeComponentPage}>
                                                            {this.state.pageNo[2]}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                    <PaginationItem>
                                                        <PaginationLink onClick={this.changeComponentPage}>
                                                            {this.state.pageNo[3]}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                    <PaginationItem>
                                                        <PaginationLink onClick={this.changeComponentPage}>
                                                            {this.state.pageNo[4]}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                    <PaginationItem>
                                                        <PaginationLink next onClick={() => this.changeComponentPage(this.state.componentPage + 1)} />
                                                    </PaginationItem>
                                                    <PaginationItem>
                                                        <PaginationLink last onClick={() => this.changeComponentPage(Math.ceil(this.props.detail.datas.length / 5))} />
                                                    </PaginationItem>
                                                </Pagination>
                                            </Col>
                                        </Row>
                                    </TabPane>
                                </TabContent>
                            </div>
                        </CardBody>
                    </Card>
                </Collapse>
            </div>
        );
    }
}

 // const sample: CarSearch = {
        //     _source: {
        //         id: 1,
        //         brandName: 'Benz',
        //         name: 'A200',
        //         price: 200000,
        //         year: 1997,
        //         detail: 'I am details',
        //         seats: 5,
        //         volume: 2000,
        //         url: "sample URL",
        //         contactName: "Wong",
        //         contactNo: "90000000",
        //         isSold: false,
        //         websiteName: "car1.hk",
        //         isAuto: false,
        //         imgLink: "imgLink"
        //     }
        // }