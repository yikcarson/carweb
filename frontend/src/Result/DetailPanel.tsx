import * as React from 'react';
import DetailComponent from './DetailComponent'
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Row, Col, Table, Pagination, PaginationItem, PaginationLink
} from 'reactstrap';
import './Css/DetailPanel.css'
import { Line, ChartData } from 'react-chartjs-2'
import * as chartjs from 'chart.js'
import { connect } from 'react-redux';
import { IRootState } from '../store';
import { CarSearch, statistics } from '../redux/Search/state';

interface IPanelProps {
    results: object | null
    statistics: statistics | null
}

interface IPanelState {
    componentPage: number
    pageNo: number[]
}

let num = 0;

class DetailPanel extends React.Component<IPanelProps, IPanelState> {
    state = {
        componentPage: null || 1,
        pageNo: [1, 2, 3, 4, 5]
    }
    // componentDidMount = () => {

    // }

    componentGenerator = (page: number = 1) => {
        let startItemIndex = page === 1 ? 0 : page * 10
        let endItemIndex = page === 1 ? 10 : page * 10 + 10
        // let slice = this.props.results!.slice(startItemIndex, endItemIndex)
        // return slice && slice.map((result: CarSearch, i) => {
        //     return <DetailComponent key={i} carDetail={result} />

        // })

    }

    changeComponentPage = (e: React.MouseEvent<any, MouseEvent> | number) => {
        let change = e > this.state.componentPage ? this.state.pageNo.map((e) => { return e + 1 }) : this.state.pageNo.map((e) => { return e - 1 }) 
        this.setState({
            pageNo: change
        })
        this.setState({
            componentPage: typeof (e) == "number" ? e : e.currentTarget.innerText
        })
    }

    pageNoUpdate = (number: number) => {
        // let change = prevState.componentPage < this.state.componentPage ? plus : minus
           
            // }) } else if (prevState.componentPage > this.state.componentPage){
            //     this.setState(prevState=>{
            //         pageNo: prevState.pageNo.map((e) => { return e - 1 })
            //     })
            // } else if(prevState.componentPage == this.state.componentPage) {
            //     return;
            // }    
    }


    render() {



        const data1: ChartData<chartjs.ChartData> = {
            labels: ['2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019'],
            datasets: [
                {
                    label: 'Car Name',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderColor: 'rgba(75,192,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [65, 59, 80, 81, 56, 55, 45, 32, 60, 80, 90]
                }
            ]
        };

        const data2: ChartData<chartjs.ChartData> = {
            labels: ['<10000', '20000', '30000', '40000', '50000', '60000', '>70000'],
            datasets: [
                {
                    label: 'My First dataset',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderColor: 'rgba(75,192,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [65, 59, 80, 81, 56, 55, 40]
                }
            ]
        };

        return (
            <div>
                <div>
                    <Row>
                        <Col xs="4">

                            <div>
                                <h2>Avg prices /w manufacture year</h2>
                                <Line data={data1} width={10}
                                    height={30}
                                    options={{ maintainAspectRatio: false }} />
                            </div>

                        </Col >
                        <Col xs="4">

                            <div>
                                <h2>Avg prices /w km</h2>
                                <Line data={data2} width={10}
                                    height={10}
                                    options={{ maintainAspectRatio: false }} />
                            </div>

                        </Col >
                        <Col xs="4">
                            <Card id="card">
                                <Table id="left-table">

                                    <tbody>
                                        <tr>
                                            <th>No of posts for this car in different websites</th>
                                            <th></th>

                                        </tr>
                                        <tr>
                                            <th scope="row">price.com.hk</th>
                                            <td>{this.props.statistics ? this.props.statistics!['price.com.hk'] : 'NA'}</td>

                                        </tr>
                                        <tr>
                                            <th scope="row">car1.hk</th>
                                            <td>{this.props.statistics ? this.props.statistics!['car1.hk'] : 'NA'}</td>

                                        </tr>
                                        <tr>
                                            <th scope="row">大昌行</th>
                                            <td>{this.props.statistics ? this.props.statistics!['大昌行'] : 'NA'}</td>

                                        </tr>
                                        <tr>
                                            <th scope="row">carlotb.com</th>
                                            <td>detail</td>

                                        </tr>
                                    </tbody>
                                </Table>
                                <CardBody>

                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>

                {/* <div> Page {this.state.componentPage} of {Math.ceil(this.props.results!.length / 10)} </div> */}

                {this.componentGenerator(this.state.componentPage)}

                {/* <button onclick={ }></button> */}
                <Pagination aria-label="Page navigation example">
                    <PaginationItem>
                        <PaginationLink first onClick={() => this.changeComponentPage(1)} />
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink previous onClick={() => this.changeComponentPage(this.state.componentPage - 1)} />
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink onClick={this.changeComponentPage}>
                            1
          </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink onClick={this.changeComponentPage}>
                            2
          </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink onClick={this.changeComponentPage}>
                            3
          </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink onClick={this.changeComponentPage}>
                            4
          </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink onClick={this.changeComponentPage}>
                            5
          </PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink next onClick={() => this.changeComponentPage(this.state.componentPage + 1)} />
                    </PaginationItem>
                    <PaginationItem>
                        {/* <PaginationLink last onClick={() => this.changeComponentPage(Math.ceil(this.props.results!.length / 10))} /> */}
                    </PaginationItem>
                </Pagination>
            </div >
        );

    }



}
const mapStateToProps = (state: IRootState) => {
    return {
        results: state.result.results,
        statistics: state.result.statistics
    }
}
export default connect(mapStateToProps, {})(DetailPanel)