import React from 'react';
import DetailPanel from './DetailPanel'
import './Css/Result.css'
import DetailComponent from './DetailComponent';

export default class Result extends React.Component {
  

  render(){
      return (
    <div className="background">
        <p> Search Results</p>
        <DetailPanel />
        
    </div>
  );
      }
}