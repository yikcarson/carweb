import React from 'react'
import { Media, Card, CardTitle, CardText, Spinner } from 'reactstrap';
import { Container, Row, Col, Button } from 'reactstrap';

//import Holder from 'react-holder'
import './Css/Panel.css'
import PanelComponent from './PanelComponent'
import { IRootState } from '../store';
import { connect } from 'react-redux';
import { criteria, CarObjects } from '../redux/Search/state';
import { push } from 'connected-react-router';
import { Dispatch } from 'redux';

 interface IntPanelProps {
    results: CarObjects | null
    criteria: criteria |null
    goToErrorPage: () => void
 }

 interface IntPanelState {

}

class InitialPanel extends React.Component<IntPanelProps,IntPanelState> {


    // PanelComponentGenerator = ()=>{
    //     let arr = Object.keys(this.props.results!)
    //         // arr.map((name)=>{
    //         //     return <PanelComponent results={this.props.results!.name} carName={name} />
    //         // })
            
    // }

    componentDidMount() {
        if (!this.props.results) {
            this.props.goToErrorPage()
        }
    }

    loadingPage= ()=>{
            if(this.props.results == null) {
                    return <Spinner color="info" />
            }
        
    }


    render() {
        
        return (
            <div >
                <div className="container">
                    
                    <Col>
                    <p>Search Results</p>
                    {this.loadingPage()}
                    {
                        this.props.results && Object.keys(this.props.results!).map((name)=>{
                            return <PanelComponent detail={this.props.results![name]} carBrand={this.props.criteria!.brand} carName={name} />
                        })
                    }
                    </Col>
               
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: IRootState) => {
    return {
        results: state.result.results,
        criteria: state.result.search
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        goToErrorPage: () => dispatch(push('/notfound'))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(InitialPanel)