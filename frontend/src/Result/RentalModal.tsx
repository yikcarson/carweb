import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, InputGroup, InputGroupAddon, Input, InputGroupText } from 'reactstrap';

interface IRentalProps {
  sendSMS:() => void
}

interface IRentalState {
    modal: boolean
    car_id:number | undefined
}

class RentalModal extends React.Component<{},IRentalState> {

    state = {
        modal: false,
        car_id: undefined
    };


    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    render() {
        return (
            <div>
                <Button color="danger" onClick={()=>this.toggle()}/>
                <Modal isOpen={this.state.modal} toggle={()=>this.toggle()} >
                    <ModalHeader toggle={()=>this.toggle()}>Request for Rental</ModalHeader>
                    <ModalBody>
                    <div>
      <InputGroup>
        <InputGroupAddon addonType="prepend">Proposed Rental Date</InputGroupAddon>
        <Input placeholder="date in DD-MM-YYYY" />
      </InputGroup>
      <br />
      <InputGroup>
        <InputGroupAddon addonType="prepend">Duration</InputGroupAddon>
        <Input placeholder="Please specify in days" />
      </InputGroup>
      <br />
      <InputGroup>
        <InputGroupAddon addonType="prepend">Offered Price</InputGroupAddon>
        <Input placeholder="suggest not to be lower than $500/day" />
      </InputGroup>
      <br />
      <InputGroup>
        <InputGroupAddon addonType="prepend">Message/Note</InputGroupAddon>
        <Input placeholder="text here" />
      </InputGroup>
      <br />
      <InputGroup>
        <InputGroupAddon addonType="prepend">User Contact</InputGroupAddon>
        <Input placeholder="your contact no." />
      </InputGroup>
      <br />
    </div>
          </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=>this.toggle()}>Do Something</Button>{' '}
                        <Button color="secondary" onClick={()=>this.toggle()}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default RentalModal;