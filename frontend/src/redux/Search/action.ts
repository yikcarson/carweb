import { Result, CarSearch, criteria, statistics, CarObjects } from './state'
import { Dispatch } from 'redux'
import { push } from 'connected-react-router';

export interface IResultsAction {
    type: '@@search/LOAD_RESULT'
    results: CarObjects | null
}

export interface ISearchAction {
    type: '@@search/SEARCH_CRITERIA'
    criteria: criteria
}

export interface IStatisticsAction {
    type: '@@search/LOAD_STATISTICS'
    statistics: statistics
}

export interface IFavoritesAction {
    type: '@@search/SAVED_FAVORITES'
    addedFavorite: {
        msg:string
        username:string
        favId:number
    }
}

export type ResultActions = IResultsAction |ISearchAction | IStatisticsAction | IFavoritesAction

export function LoadResult(results: object[]) {
    
    return {
        type: '@@search/LOAD_RESULT',
        results: results
    }
}

export function LoadStatistics(statistics: object) {
    
    return {
        type: '@@search/LOAD_STATISTICS',
        statistics: statistics
    }
}

export function SearchCriteria(criteria: object) {
    
    return {
        type: '@@search/SEARCH_CRITERIA',
        criteria: criteria
    }
}

export function favRecords(record:{
    msg:string
    username:string
    favId:number
}) {
    return {
        type:'@@search/SAVED_FAVORITES',
        addedFavorite: record
    }
}

export function fetchResult(criteria: object) {
    
    return async (dispatch: Dispatch) => {
        dispatch(push('/loading'))
        const res = await fetch(`${process.env.REACT_APP_SERVER}/search/quick`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({ criteria })
        })
        const json = await res.json();
        //console.log(json);
        if(!json) {
            return dispatch(push('/notfound'));
        } else {
            dispatch(SearchCriteria(criteria))
            dispatch(LoadResult(json))
            // dispatch(LoadStatistics(json.amount))
            dispatch(push('/result'))
        }
        
    }
}

export function saveFavorites(username:string|null, car_id:number) {
    return async (dispatch: Dispatch) => {
        const res = await fetch(`${process.env.REACT_APP_SERVER}/profile/fav`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({ 
                username:username,
                car_id: car_id
            })
        })
        const json = await res.json();
        console.log(json);
        if(json){
        dispatch(favRecords(json))
        }
    }
}