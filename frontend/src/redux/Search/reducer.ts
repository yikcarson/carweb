import { IResultState, Result } from './state';
import { ResultActions } from './action';

let initialState: IResultState = {
    search: null,
    results: null,
    statistics: null,
    addedFavorites: []
}


export const resultReducer = (state: IResultState = initialState, action: ResultActions) => {
    switch (action.type) {
        case '@@search/LOAD_RESULT':
            return {
                ...state,
                results: action.results
            }
        case '@@search/SEARCH_CRITERIA':
            return {
                ...state,
                search: action.criteria
            }
        case '@@search/LOAD_STATISTICS':
            return {
                ...state,
                statistics: action.statistics
            }
        case '@@search/SAVED_FAVORITES':
                const newFavorites = state.addedFavorites.slice()
                newFavorites.splice(newFavorites.length, 0, action.addedFavorite)
            return {
                ...state,
                addedFavorites: newFavorites
            }

    }
    return state;

}
