import { number } from "prop-types";

export interface Result {
    brand: string
    name: string
    year: number
    seats: number
    volume: number
    isAuto: boolean
    detail: string
    contactName: string
    contactNo: number
    price: number
    updateDate: Date
    img1: string | null
    img2: string | null
    img3: string | null
    img4: string | null
    websiteName: string
    url: string
    isSold: boolean

}

export interface ISearchState {
    search: criteria | null
}

export interface criteria {
    brand: string | undefined
    seats: number | null | undefined
    startPrice: number | null | undefined
    endPrice: number | null | undefined
    isAuto: boolean | null | undefined
}

export interface statistics {
    "price.com.hk": number,
    "car1.hk": number,
    "大昌行": number
}

export interface addedFav {
    msg:string
    username:string
    favId:number
}

export interface IResultState {
    search:criteria | null
    results: CarObjects | null
    statistics: statistics| null
    addedFavorites: addedFav[]
}

export interface carObject {
    datas:[]
    amount: number
        label: []
        prices: []
}


export interface CarObjects {
    [key: string]: CarDetail
}

export interface CarDetail {
    datas: CarSearch[]
    amount: number,
    label: number[],
    prices: number[]
}



export interface CarSearch {
    _source: {
        id: number,
        brandName: string,
        name: string,
        price: number,
        year: number,
        detail: string,
        seats: number | null,
        volume: number | null,
        url: string,
        contactName: string,
        contactNo: string,
        isSold: boolean,
        km:number
        websiteName: string
        isAuto: boolean
        imgLink: string
        updateDate:string
        //rental
        rental:boolean
        car_id:number
        RentalPrice?:number
        start_date?:Date
        days?:number

    }
}
