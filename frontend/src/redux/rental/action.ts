import { rentalData } from "./state";
import { Dispatch } from 'redux';
import axios from 'axios';
import { push } from "connected-react-router";

export interface IRentalGetAction {
    type: "@@IRental/GetRentalCar"
    data: rentalData[]
}

export type IRentalAction = IRentalGetAction


export function updateRentalCar(data: rentalData[]): IRentalGetAction {
    console.log('update')
    return {
        type: '@@IRental/GetRentalCar',
        data
    }
}

export function getRentalCar() {
    return async (dispatch: Dispatch<IRentalAction>) => {
        const res = await axios({
            method: "GET",
            url: `${process.env.REACT_APP_SERVER}/rental/car`
        })
        const data = res.data
        console.log(data)
        dispatch(updateRentalCar(data))
    }
}

export function redirect(patch: string) {
    return async (dispatch: Dispatch) => {
        dispatch(push(patch))
    }
}