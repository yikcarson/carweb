export interface IRentalState {
    data:rentalData[]
}

export interface rentalData {
    // id: number,
    brandName: string,
    name: string,
    price: number,
    year: number,
    detail: string,
    seats: number | null,
    volume: number | null,
    // url: string,
    contactName: string,
    contactNo: string,
    // isSold: boolean,
    km: number
    // websiteName: string
    isAuto: boolean
    imgLink: string
    updateDate: string
    rental: boolean
    RentalPrice: number
    days: number
    car_id:number
    username:string
}