import { IRentalState } from './state';
import { IRentalAction } from "./action";

const initalState: IRentalState = {
    data: []
}

export const rentalReducer = (state: IRentalState = initalState, action: IRentalAction) => {
    switch (action.type) {
        case '@@IRental/GetRentalCar':
            return {
                ...state,
                data: action.data
            }
    }
    return state
}