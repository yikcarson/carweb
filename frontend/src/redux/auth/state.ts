export interface IAuthState {
    isAuth: boolean | null,
    username: string | null
}