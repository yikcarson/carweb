import { IAuthState } from './state';
import { IAuthActions } from './action';


const initialState: IAuthState = {
    isAuth: null,
    username: null
}
export const authReducer = (state: IAuthState = initialState, action: IAuthActions) => {
    switch (action.type) {
        case '@@auth/LoginSuccess':
            return {
                ...state,
                isAuth: true,
                username: action.username
            }
        case '@@auth/LoginFaile':
            return {
                ...state,
                isAuth: null,
                username: null
            }
        case '@@auth/LogOut':
            return {
                ...state,
                isAuth: false,
            }
    }
    return state
}