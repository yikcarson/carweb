import { Dispatch } from 'redux'

export interface ILoginSuccessAction {
    type: '@@auth/LoginSuccess',
    username: string
}
export interface ILoginFaileAction {
    type: '@@auth/LoginFaile'
}
export interface ILogOutAction {
    type: '@@auth/LogOut'

}

export type IAuthActions = ILogOutAction | ILoginFaileAction | ILoginSuccessAction


export function loginSuccess(username: string): ILoginSuccessAction {
    return {
        type: '@@auth/LoginSuccess',
        username,
    }
}
export function loginFaile(): ILoginFaileAction {
    return {
        type: '@@auth/LoginFaile',
    }
}
export function logOut(): ILogOutAction {
    localStorage.removeItem('token')
    return {
        type: '@@auth/LogOut'
    }
}

export function loginLocal(username: string, password: string) {
    return async (dispatch: Dispatch<IAuthActions>) => {
        const res = await fetch(`${process.env.REACT_APP_SERVER}/login/local`, {
            method: "POST",
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify({ username, password })
        })
        const result = await res.json()
        if (res.status == 401) {
            dispatch(loginFaile())
            return false
        } else {
            localStorage.setItem('token', result.token)
            dispatch(loginSuccess(result.username))
            return true

        }
    }
}
export function restoreLogin() {
    return async (dispatch: Dispatch) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_SERVER}/restoreLogin`, {
                method: "Get",
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
            const result = await res.json()
            dispatch(loginSuccess(result.username))
        } catch (error) {
            if (error.response && error.response.status == 401) {
                localStorage.clear()
                dispatch(logOut())
            } else {
                dispatch(logOut())
            }
        }
    }
}

export function loginFacebook(accessToken: string) {
    return async (dispatch: Dispatch) => {
        const res = await fetch(`${process.env.REACT_APP_SERVER}/login/facebook`, {
            method: 'Post',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify({ accessToken })
        })
        const result = await res.json()
        if (res.status == 401) {
            dispatch(loginFaile())
            return false
        } else {
            localStorage.setItem('token', result.token)
            dispatch(loginSuccess(result.username))
            return true
        }
    }
}