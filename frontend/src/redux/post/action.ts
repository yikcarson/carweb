
export interface IPostUpdateBrand {
    type: "@@IPost/UpdateBrand",
    brand: string
}
export interface IPostUpdateCarname{
    type: "@@IPost/UpdateCarname",
    carname: string
}

export type IPostAction = IPostUpdateBrand | IPostUpdateCarname

export function postUpdateBrand(brand: string): IPostUpdateBrand {
    return {
        type: "@@IPost/UpdateBrand",
        brand,
    }
}
export function postUpdateCarname(carname: string): IPostUpdateCarname {
    return {
        type:  "@@IPost/UpdateCarname",
        carname,
    }
}
