import { IPostStatus } from './status';
import { IPostAction } from './action';

const initialState: IPostStatus = {
    brand: 'Audi',
    carname: ''
}

export const postReducer = (state: IPostStatus = initialState, action: IPostAction) => {
    switch (action.type) {
        case "@@IPost/UpdateBrand":
            return {
                ...state,
                brand: action.brand
            }
        case "@@IPost/UpdateCarname":
            return {
                ...state,
                carname:action.carname
            }

    }
    return state
}