import React from 'react'
import { connect } from 'react-redux';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap'
import classnames from 'classnames';
import { IRootState } from '../store';
import DetailComponent from '../Result/DetailComponent';
import axios from 'axios';
import RentedComponent from './RentedComponent';

interface IProFilesState {
    activeTab: string
    postData: any;
    bookmarkData: any
    rentalData:any
}

interface IProFilesProps {
    username: string | null
}

class Profile extends React.Component<IProFilesProps, IProFilesState> {
    state: IProFilesState = {
        activeTab: "1",
        postData: [],
        bookmarkData: [],
        rentalData:[]
    }
    componentDidMount = async () => {
        const postData = await axios.get(`${process.env.REACT_APP_SERVER}/profile/post/${this.props.username}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        const bookmarkData = await axios.get(`${process.env.REACT_APP_SERVER}/profile/fav/${this.props.username}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        const rentalData = await axios.get(`${process.env.REACT_APP_SERVER}/profile/rental/${this.props.username}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        return this.setState({
            postData: postData.data,
            bookmarkData: bookmarkData.data,
            rentalData:rentalData.data?rentalData.data:[]
        })
    }
    toggle = (tab: string) => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    render() {


        { console.log(this.state) }

        return (
            <div id="profile">
                <div className="profile">
                    <div className="userInfo">
                        <div id="userIcon"></div>
                        <div id="userName"><span>{this.props.username}</span></div>

                    </div>
                    <div className="content">
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '1' })}
                                    onClick={() => { this.toggle('1'); }}
                                >
                                    賣車記錄
            </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '2' })}
                                    onClick={() => { this.toggle('2'); }}
                                >
                                    關注車輛
            </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '3' })}
                                    onClick={() => { this.toggle('3'); }}
                                >
                                    已租車輛
            </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <Row>
                                    <Col sm="12">
                                        {this.state.postData.map((data: any) => {
                                            return <DetailComponent carDetail={{ "_source": data }} />
                                        })}

                                    </Col>
                                </Row>
                            </TabPane>
                            <TabPane tabId="2">
                                <Row>
                                    <Col sm="12">
                                        {this.state.bookmarkData.map((data: any) => {
                                            return <DetailComponent carDetail={{ "_source": data }} />
                                        })}
                                    </Col>
                                </Row>
                            </TabPane>
                            <TabPane tabId="3">
                                <Row>
                                    <Col sm="12">
                                        {console.log(this.state)}
                                        {this.state.rentalData.map((data: any) => {
                                            return <RentedComponent carDetail={{ "_source": data }} />
                                        })}
                                    </Col>
                                </Row>
                            </TabPane>
                          
                        </TabContent>
                    </div>
                </div>
            </div>
        )
    }

}
const mapStateToProps = (state: IRootState) => ({
    username: state.auth.username
})

const mapDispatchToProps = () => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(Profile)