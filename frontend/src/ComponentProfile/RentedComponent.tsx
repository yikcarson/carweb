import './RentedComponent.css'
import React from 'react'
import { Media, CardTitle, CardText, Nav, NavItem, NavLink, TabContent, TabPane, UncontrolledCarousel, Collapse, Popover, PopoverHeader, PopoverBody } from 'reactstrap';
import { Container, Row, Col, Table } from 'reactstrap';
import { UncontrolledCollapse, Button, CardBody, Card } from 'reactstrap';
import car1 from './720s_spider.jpg'
import car2 from './BENZ_AMG.jpg'
import defaultImage from './default.png'
import classnames from 'classnames';
import { connect } from 'react-redux';
import { IRootState } from '../store';
import { CarSearch, addedFav } from '../redux/Search/state';
import moment from 'moment'
import { saveFavorites} from '../redux/Search/action'
import { NavLink as NavLinkRRD } from 'react-router-dom'

interface IComponentProps {
  // num:number
  // results: object[]
  carDetail: CarSearch
  user_name: string |null
//   addedFav: addedFav[] |null
//   saveFavorites: (user_name: string|null, car_id:number)=>void
}

interface IComponentState {
 activeTab: string
 collapse:boolean
popoverOpen:boolean
}

class RentedComponent extends React.Component<IComponentProps, IComponentState> {

  state = {
    activeTab: "2",
    collapse: false,
    popoverOpen:false
  }

  toggle(tab: string) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  toggleCollapse() {
    this.setState(prevState => ({ 
      collapse: !prevState.collapse 
    }));
  }

  togglePopover(){
    this.setState(prevState=>({
      popoverOpen: !prevState.popoverOpen
    }));


  }

  render() {
    const { carDetail } = this.props
    let details = {
      brand: carDetail._source.brandName,
      name: carDetail._source.name,
        price: carDetail._source.price,
      year: carDetail._source.year,
      detail: carDetail._source.detail,
      auto: carDetail._source.isAuto,
      seats: carDetail._source.seats,
      volume: carDetail._source.volume,
      url: carDetail._source.url,
      contactName: carDetail._source.contactName,
      contactNo: carDetail._source.contactNo,
      isSold: carDetail._source.isSold,
      km: carDetail._source.km,
      websiteName: carDetail._source.websiteName,
      imgLink: carDetail._source.imgLink,
      updateDate: carDetail._source.updateDate,
      //rental
      rental:carDetail._source.rental?carDetail._source.rental:undefined,
      car_id:carDetail._source.car_id?carDetail._source.car_id:undefined,
        //rental record
        RentalPrice: carDetail._source.RentalPrice,
        startDate:carDetail._source.start_date,
        duration:carDetail._source.days

    }

    if (this.state.popoverOpen === true) {
      window.setTimeout(()=>{this.setState({
        popoverOpen: false
      })}, 2000)
    }
  
    return (
      <div >
        <div className="container">
          <Media>
            <Media body className="detail-com-body">
              <Row className="big-row-component">
                <Col xs="3" className="pic">
                  <img
                    src={details.imgLink ? details.imgLink : defaultImage} width={190} height={160} alt="car1" />
                </Col>
                <Col xs="7" className="mid-col">
                  <Row className="car-name-core">{details.name}</Row>

                  <Row className="car-price-core">Rental Price: ${details.RentalPrice}</Row>
                  <div style={{ height: '85px', width: '100%', overflowY: 'auto', overflowX: 'hidden', margin: '0px -15px', padding: '0px 15px' }}>
                    <Row className="desc-core">Manufacture Year: {details.year}</Row>

                    <Row className="desc-detail">Detail:&nbsp;&nbsp;{details.detail}</Row>
                    <Row className="desc-core">From: {details.websiteName} </Row>

                    <Row className="desc-core">Sold: {details.isSold ? "sold" : "still listing"} </Row>
                    <Row className="desc-core">update Date: {moment(details.updateDate).format('DD-MM-YYYY')}</Row>
                  </div>
                  <Row className="space"></Row>
                  <Row className="more-details-toggle">
                  <Button color="primary" className="detail-toggle"onClick={()=>this.toggleCollapse()} style={{ marginBottom: '1rem' }}>
                    Details
    </Button>
    </Row>
                </Col>
                <Col xs="2" className="right-button">
                  <div className="start-date">Rental Start Date:<br />{moment(details.startDate).format("DD-MM-YYYY")}</div>
                  <div className="space"></div>
                  <div className="duration">Duration: {details.duration} days</div>
                  <div>
                 
        
      </div>
      <div className="space"></div>
                  
                  
                </Col>
              </Row>
            </Media>
          </Media>
        </div>
        <div >

          <Collapse isOpen={this.state.collapse}>
            <Card>
              <CardBody>
                <div>
                  <Nav tabs>
                  
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === '2' })}
                        onClick={() => { this.toggle('2'); }}
                      >
                        Details
            </NavLink>
                    </NavItem>
                  </Nav>
                  <TabContent activeTab={this.state.activeTab}>
                    
                    <TabPane tabId="2">
                      <Row>

                        <Col sm="12">

                          <Table striped id="detail-table">
                            <thead>
                              <tr>
                                <th>Name</th>
                                <th>{details.name}</th>

                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <th scope="row">Manufacture Year</th>
                                <td>{details.year}</td>
                              </tr>
                              <tr>
                                <th scope="row">km</th>
                                <td>30000</td>
                              </tr>
                              <tr>
                                <th scope="row">Seats</th>
                                <td>{details.seats}</td>
                              </tr>
                              <tr>
                                <th scope="row">Engine Volume</th>
                                <td>{details.volume}cc</td>
                              </tr>


                              <tr>
                                <th scope="row">isSold</th>
                                <td>{details.isSold ? "sold" : "still listing"}</td>
                              </tr>
                              <tr>
                                <th scope="row">url</th>
                                <td><a href={details.url}>Link</a></td>
                              </tr>
                              <tr>
                                <th scope="row">Contact Name</th>
                                <td>{details.contactName}</td>
                              </tr>
                              <tr>
                                <th scope="row">Contact No</th>
                                <td>{details.contactNo}</td>
                              </tr>
                              <tr>
                                <th scope="row">Update Date</th>
                                <td>{moment(details.updateDate).format('DD-MM-YYYY')}</td>
                              </tr>
                            </tbody>
                          </Table>

                        </Col>
                      </Row>
                    </TabPane>
                  </TabContent>
                </div>
              </CardBody>
            </Card>
          </Collapse>
        </div>
      </div>
    );
  }
}
 const mapStateToProps = (state:IRootState) => {
   return {
       //results: state.result.results
      user_name: state.auth.username,
    //   addedFav: state.result.addedFavorites
   }
 }

const mapDispatchToProps = {
  
}
 export default connect(mapStateToProps, mapDispatchToProps)(RentedComponent)