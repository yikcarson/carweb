import { RouterState, connectRouter, routerMiddleware } from 'connected-react-router';
import thunk, { ThunkDispatch } from 'redux-thunk';
import { createBrowserHistory } from 'history';
import { combineReducers, compose, createStore, applyMiddleware } from 'redux';
import { IAuthActions } from './redux/auth/action';
import { IAuthState } from './redux/auth/state';
import { authReducer } from './redux/auth/reducer';
import { ResultActions } from './redux/Search/action';
import { IResultState } from './redux/Search/state';
import { resultReducer } from './redux/Search/reducer';
import { IPostAction } from './redux/post/action';
import { IPostStatus } from './redux/post/status';
import { postReducer } from './redux/post/reducer';
import { IRentalAction } from './redux/rental/action';
import { IRentalState } from './redux/rental/state';
import { rentalReducer } from './redux/rental/reducer';

export const history = createBrowserHistory();

type IRootAction =  IAuthActions | ResultActions |IPostAction |IRentalAction

export type ThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>

export interface IRootState {
  rental:IRentalState,
  auth:IAuthState,
  post:IPostStatus
  result:IResultState
  router: RouterState
}

const rootReducer = combineReducers<IRootState>({
  rental:rentalReducer,
  auth:authReducer,
  result:resultReducer,
  post:postReducer,
  router: connectRouter(history)
})

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore<IRootState, IRootAction, {}, {}>(
  rootReducer,
  composeEnhancers(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history))
  ));
