import React from 'react'
import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col } from 'reactstrap'
import classnames from 'classnames';
import DetailComponent from '../Result/DetailComponent';
import { connect } from 'react-redux';
import { IRootState, ThunkDispatch } from '../store';
import { getRentalCar } from '../redux/rental/action';
import RentalComponent from './RentalComponent';
interface IProFilesState {
    activeTab: string
}
interface IProFilesProps {
    data: any;
    getRentalCar: () => void
}

class Rental extends React.Component<IProFilesProps, IProFilesState> {
    state: IProFilesState = {
        activeTab: "1",
    }
    componentDidMount = () => {
        this.props.getRentalCar()
     }

    toggle = (tab: string) => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    render() {
        return (
            <div>
                <Nav tabs>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '1' })}
                            onClick={() => { this.toggle('1'); }}
                        >
                            可租用車輛
                            
            </NavLink>
                    </NavItem>

                </Nav>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <Row>
                            <Col sm="12">
                                {this.props.data.map((data: any) => {
                                    return <RentalComponent carDetail={{ "_source": data }} />
                                })}

                            </Col>
                        </Row>
                    </TabPane>
                </TabContent>
            </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => ({
    data: state.rental.data
})
const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    getRentalCar: () => dispatch(getRentalCar())
})

export default connect(mapStateToProps, mapDispatchToProps)(Rental)