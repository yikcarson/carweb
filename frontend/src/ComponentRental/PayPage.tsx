import * as React from 'react';
import StripeCheckout, { Token } from 'react-stripe-checkout'
import { rentalData } from '../redux/rental/state';
import { connect } from 'react-redux';
import { IRootState, ThunkDispatch } from '../store';
import { DropdownItem } from 'reactstrap';
import axios from 'axios';
import DatePicker from "react-datepicker";
import { ToastsContainer, ToastsStore, ToastsContainerPosition } from 'react-toasts'
import { redirect } from '../redux/rental/action';
interface IPaypageState {
    rentalCarName: string;
    imgLink: string;
    username: string;
    Days: number;
    price: number;
    car_id: number;
    startDay: Date
}
interface IPaypageProps {
    rentalDatas: rentalData[]
    redirect: () => void
}
class PayPage extends React.Component<IPaypageProps, IPaypageState> {
    state: IPaypageState = {
        rentalCarName: '',
        imgLink: '',
        username: '',
        Days: 0,
        price: 0,
        car_id: 0,
        startDay: new Date()
    }

    componentDidMount = () => {
        const carId = parseInt(window.location.search.split('=')[1])
        // const a = new URLSearchParams(window.location.search)
        // a.get('id')
        const carInfo = this.props.rentalDatas.filter(data => data.car_id === carId)[0]
        console.log(carInfo)
        this.upDate(carInfo.name, carInfo.imgLink, carInfo.username, carInfo.days, carInfo.RentalPrice, carInfo.car_id)
        console.log(this.state)
    }
    upDate = (rentalCarName: string, imgLink: string, username: string, Days: number, price: number, car_id: number) => {
        return this.setState({
            rentalCarName,
            imgLink,
            username,
            Days,
            price,
            car_id,
        })
    }
    handleChange = (date: Date) => {
        this.setState({
            startDay: date
        });
    }

    toast = (content: string) => {
        ToastsStore.success(content)
    }

    handleToken = async (token: Token) => {
        const res = await axios.post(`${process.env.REACT_APP_SERVER}/rental/pay`, { token, product: this.state }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        }
        )
        console.log(res)
        if (res.status === 200) {
            this.toast(`${res.data["msg"]}`)
            setTimeout(() => (this.props.redirect()), 2000)

        }
    }

    render() {
        return (

            <div className='PayPage'>
                <div id='PayPageBox'>
                    <div id='PayPageLeft'>
                        <div id='PayPageTittle'>
                            <div>{this.state.rentalCarName}</div>
                        </div>
                        <DropdownItem divider />
                        <div id='PayPagepic'>
                            <img src={this.state.imgLink} height='auto' width='200px'></img>
                        </div>
                    </div>

                    <div id='PayPageRight'>
                        <div id='rentalCarProvider'><div><span>username:</span></div> {this.state.username} </div>
                        <DropdownItem divider />
                        <div id='rentalCarDays'><span>Days:</span> {this.state.Days}</div>
                        <DropdownItem divider />
                        <div id='rentalCarPrice'><span>price $:</span> {this.state.price} </div>
                        <DropdownItem divider />
                        <div id='rentalStartDate'> StartDate:<DatePicker
                            selected={this.state.startDay}
                            onChange={this.handleChange}
                        /></div>
                        <DropdownItem divider />
                        <div id='Pay_button'>
                            <StripeCheckout
                                stripeKey={`${process.env.REACT_APP_stripe_key}`}
                                token={(token) => this.handleToken(token)}
                                amount={this.state.price * 100}
                                currency='HKD'
                                name="carlotb"
                                panelLabel=''

                            />
                        </div>
                    </div>
                </div>
                < ToastsContainer lightBackground={true} position={ToastsContainerPosition.TOP_RIGHT} store={ToastsStore} />

            </div>
        )
    }
}
const mapStateToProps = (state: IRootState) => ({
    rentalDatas: state.rental.data
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    redirect: () => { dispatch(redirect('/rental')) }
})

export default connect(mapStateToProps, mapDispatchToProps)(PayPage)