import React from 'react';
import './css/App.css';
import NavbarTitle from './ComponentNarbar/Navbar';
import { Provider } from 'react-redux';
import store, { history } from './store';
import { ConnectedRouter } from 'connected-react-router';
import { NavLink, Route, Switch } from 'react-router-dom';
import Profile from './ComponentProfile/Profile';
import DetailPanel from './Result/DetailPanel'
import { PrivateRoute } from './PrivateRoute';
import PostPage from './ComponentPostCar/PostPage';
import 'react-inputs-validation/lib/react-inputs-validation.min.css';
import Panel from './Result/Panel'
import NotFound from './Result/NotFound';
import { Notlogin } from './Notlogin';
import { IndexPage } from './Main';
// import RentalModal from './Result/RentalModal';
import News from './News/News'
import Rental from './ComponentRental/Rental';
import PayPage from './ComponentRental/PayPage';
import SearchPage from './Search/SearchPage';
import Loading from './Result/Loading';

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <div className="App">
          <div id="Top-test">
            <NavbarTitle></NavbarTitle>
          </div>
          <Switch>
            {/* <Route path='/' exact={true} component={} /> */}
            <PrivateRoute path='/post' exact={true} component={PostPage} />
            <PrivateRoute path='/rental/pay' exact={true} component={PayPage} />
            <Route path='/index' exact={true} component={IndexPage} />
            <Route path='/' exact={true} component={IndexPage} />
            <Route path='/login' exact={true} component={Notlogin}/>
            <Route path='/search' exact={true} component={SearchPage} />
            <Route path='/profile' exact={true} component={Profile} />
            <Route path='/result' exact={true} component={Panel} />
            <Route path='/result/detail' exact={true} component={DetailPanel} />
            <Route path='/rental' exact={true} component={Rental} />
            <Route path='/notfound' exact={true} component={NotFound} />
            <Route path='/loading' exact={true} component={Loading} /> 
            <Route path='/news' exact={true} component={News} />
          </Switch>
        </div>
      </ConnectedRouter>
    </Provider>
  );
}

export default App;
