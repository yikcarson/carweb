import React from 'react'
import { Collapse, Navbar, NavbarToggler, Button, Nav, NavItem, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import Carlotb from '../img/logo_transparent.png'
import { NavLink, } from 'react-router-dom';
import ModalTabs from './ModalTabs';
import { connect } from 'react-redux';
import { IRootState, ThunkDispatch } from '../store';
import { restoreLogin, logOut } from '../redux/auth/action';

interface INavbarTitleState {
    isOpen: boolean
    modal: boolean,
    dropdownOpen: boolean
}

interface INavbarTitleProps {
    isLogin: boolean | null,
    username: string | null,
    restoreLogin: () => void,
    logOut: () => void
}

class NavbarTitle extends React.Component<INavbarTitleProps, INavbarTitleState> {
    state: INavbarTitleState = {
        isOpen: false,
        modal: false,
        dropdownOpen: false
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
    dropDownToggle = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }
    ModalToggle = () => {
        this.setState({
            modal: !this.state.modal
        })
    }
    componentDidMount() {
        this.props.restoreLogin()
    }
    render() {
        return (
            <div id="Narvar-Top">
                <Navbar color="light" light expand="md">
                    <NavLink to={'/index'}><img src={Carlotb} /></NavLink>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink to={'/post'}><Button color="secondary">賣車</Button></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink to={'/search'}><Button color="secondary">二手車資訊</Button></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink to={'/news'}><Button color="secondary">Car News</Button></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink to={'/rental'}><Button color="secondary">租車</Button></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink hidden={this.props.isLogin == false || null ? false : true} to={'#'} onClick={this.ModalToggle}><Button color="secondary">登入/註冊</Button></NavLink>
                                <Dropdown hidden={this.props.isLogin == true ? false : true} isOpen={this.state.dropdownOpen} toggle={this.dropDownToggle}>
                                    <DropdownToggle caret>
                                        {this.props.username}
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        <NavLink to={'/profile'}><DropdownItem>Profile</DropdownItem></NavLink>
                                        <NavLink to={'/'} onClick={this.props.logOut}><DropdownItem>Log out</DropdownItem></NavLink>
                                    </DropdownMenu>
                                </Dropdown>
                            </NavItem>
                            <ModalTabs modal={this.state.modal} Toggle={this.ModalToggle} />
                        </Nav>
                    </Collapse>
                </Navbar>
            </div >
        )
    }
}



const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    restoreLogin: () => dispatch(restoreLogin()),
    logOut: () => dispatch(logOut())
})

export default connect(
    (state: IRootState) => ({
        isLogin: state.auth.isAuth,
        username: state.auth.username
    }), mapDispatchToProps
)(NavbarTitle)