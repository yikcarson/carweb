import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Button, Row, Col, Modal, ModalBody, InputGroup, InputGroupAddon, InputGroupText, Input } from 'reactstrap';
import classnames from 'classnames'
import facebook from '../img/facebook.svg'
import register from '../img/register.svg'
import login from '../img/login.svg'
import google from '../img/google.svg'
import { NavLink as NavLinkReact } from 'react-router-dom';
import eye from '../img/eye.svg'
import { connect } from 'react-redux';
import { ThunkDispatch, IRootState } from '../store';
import { loginLocal, loginFacebook } from '../redux/auth/action';
import FacebookLogin, { ReactFacebookLoginInfo } from 'react-facebook-login'


interface ITabsState {
    activeTab: string
    username: string
    password: string
    hidden: boolean
}
interface ITabsProps {
    Toggle: () => void,
    modal: boolean,
    login: (username: string, password: string) => void,
    isLogin: boolean | null,
    loginFacebook: (accessToken: string) => void
}

class Tabs extends React.Component<ITabsProps, ITabsState> {
    state: ITabsState = {
        activeTab: 'login',
        username: '',
        password: '',
        hidden: true
    }

    toggle = (tab: string) => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    clean = () => {
        this.setState({
            username: '',
            password: ''
        })
    }
    changeUsername = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            username: event.target.value
        })
    }
    changePassword = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            password: event.target.value
        })
    }
    changeType = () => {
        this.setState({
            hidden: !this.state.hidden
        })
    }
    createUser = async () => {
        const username = this.state.username
        const password = this.state.password
        if (username == '' || password == '') {
            return alert('Enter username and password')
        }
        const res = await fetch(`${process.env.REACT_APP_SERVER}/user/localuser`, {
            method: "Post",
            headers: { "content-type": "application/json" },
            body: JSON.stringify({ username, password })
        })
        const result = await res.json()
        if (res.status == 200) {
            return alert('Create Success')
        } else {
            return alert(result)
        }
    }
    loginLocal = async () => {
        const username = this.state.username
        const password = this.state.password
        if (username && password) {
            await this.props.login(username, password)
            if (!this.props.isLogin) {
                this.clean()
                return alert('wrong username or password')
            }
            return this.props.Toggle()
        } else {
            return alert('enter password & username ')
        }
    }

    fBOnCLick() {
        return null;
    }

    fBCallback = async (userInfo: ReactFacebookLoginInfo & { accessToken: string }) => {
        if (userInfo.accessToken) {
            this.clean()
            await this.props.loginFacebook(userInfo.accessToken);
            if (this.props.isLogin) {
                this.props.Toggle()
            }
        }
        return null;
    }
    render() {
        return (
            <div>
                <Modal isOpen={this.props.modal} toggle={this.props.Toggle} className='ModalTabs'>
                    <br />
                    <Nav tabs>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === 'login' })}
                                onClick={() => { this.toggle('login'); this.clean() }}
                            ><img src={login} className='NavIcon' />
                                登入
            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === 'register' })}
                                onClick={() => { this.toggle('register'); this.clean() }}
                            ><img src={register} className='NavIcon' />
                                註冊
            </NavLink>
                        </NavItem>
                    </Nav>
                    <ModalBody>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="login">
                                <Row>
                                    <Col sm="12">
                                        <div className='login'>
                                            <div className='loginInof'>
                                                <InputGroup>
                                                    <InputGroupAddon addonType="prepend">username</InputGroupAddon>
                                                    <Input onChange={this.changeUsername} value={this.state.username} />
                                                </InputGroup>
                                                <br />
                                                <InputGroup>
                                                    <InputGroupAddon addonType="prepend">password</InputGroupAddon>
                                                    <Input type={this.state.hidden ? 'password' : 'text'} onChange={this.changePassword} value={this.state.password} />
                                                    <InputGroupAddon addonType="append">
                                                        <InputGroupText onClick={this.changeType}><img src={eye} className='eye' /></InputGroupText>
                                                    </InputGroupAddon>
                                                </InputGroup>
                                            </div>
                                            <div className='loginButton'>
                                                <FacebookLogin
                                                    appId={process.env.REACT_APP_Facebook || ''}
                                                    autoLoad={false}
                                                    fields="name,email,picture"
                                                    onClick={this.fBOnCLick}
                                                    callback={this.fBCallback}
                                                    icon={<img src={facebook} />}
                                                    textButton={''}
                                                    size='small'
                                                />
                                                <NavLinkReact to={'/login/facebook'} className='socialLogin'><img src={google} ></img></NavLinkReact>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm="12">
                                        <div className='ModalFootButton'>
                                            <Button color="secondary" onClick={this.loginLocal}>sign in</Button>
                                            <Button color="secondary" onClick={this.props.Toggle}>Cancel</Button>
                                        </div>
                                    </Col>
                                </Row>
                            </TabPane>
                            <TabPane tabId="register">
                                <Row>
                                    <Col sm="12">
                                        <InputGroup>
                                            <InputGroupAddon addonType="prepend">username</InputGroupAddon>
                                            <Input onChange={this.changeUsername} value={this.state.username} />
                                        </InputGroup>
                                        <br />
                                        <InputGroup>
                                            <InputGroupAddon addonType="prepend">password</InputGroupAddon>
                                            <Input type={this.state.hidden ? 'password' : 'text'} onChange={this.changePassword} value={this.state.password} />
                                            <InputGroupAddon addonType="append">
                                                <InputGroupText onClick={this.changeType}><img src={eye} className='eye' /></InputGroupText>
                                            </InputGroupAddon>
                                        </InputGroup>

                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm="12">
                                        <div className='ModalFootButton'>
                                            <Button color="secondary" onClick={() => { this.createUser(); this.clean() }}>Submit</Button>
                                            <Button color="secondary" onClick={this.props.Toggle}>Cancel</Button>
                                        </div>
                                    </Col>
                                </Row>

                            </TabPane>
                        </TabContent>
                    </ModalBody>

                </Modal>
            </div>
        );
    }
}

const mapStateToProps = (state: IRootState) => ({
    isLogin: state.auth.isAuth
})

const mapDispatchToprops = (dispatch: ThunkDispatch) => ({
    login: (username: string, password: string) => dispatch(loginLocal(username, password)),
    loginFacebook: (accessToken: string) => dispatch(loginFacebook(accessToken))
})

export default connect(mapStateToProps, mapDispatchToprops)(Tabs)