import React from 'react'
import { NavLink } from 'react-router-dom';

export class IndexPage extends React.Component {
    render() {
        return (
            <div className="IndexBackgroud IndexContainer">
                <div className='fullscreen-video-wrap'>
                    <video src={require('./video/a7a.mp4')} autoPlay={true} loop={true} muted />
                </div>
                <div className="contentOverlay"></div>
                <div className="videoContentArea">
                    <div className='videoContent'>
                        <h1>Hello Every One</h1>
                        <p>Here is CarlotB.com</p>
                   <NavLink to={'/search'}><button id='indexBtn'> Let's Start</button></NavLink>     
                    </div>
                </div>
            </div>
        )
    }
}